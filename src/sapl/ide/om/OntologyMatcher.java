package sapl.ide.om;

import org.semanticweb.owl.align.Alignment;

import sapl.core.SaplModel;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.3 (17.03.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 * */

public interface OntologyMatcher {
	public Alignment match(SaplModel model1, SaplModel model2) throws Exception;
}
