package sapl.ide.om;

import java.io.File;
import java.net.URI;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.semanticweb.owl.align.Alignment;

import fi.vtt.ict.IoT.ase.ASE;
import sapl.core.rab.RABUtils;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.sapln3.SaplN3Producer;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.ide.Activator;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.5 (14.08.2017)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015-2017, VTT
 * 
 * */

public class ASEMatcher implements OntologyMatcher{

	@Override
	public Alignment match(SaplModel model1, SaplModel model2) throws Exception {

		SaplN3ProducerOptions options = new SaplN3ProducerOptions(true, false, true, false, false, false);
		SaplN3Producer producer1 = new SaplN3Producer(model1, model1.prefixer, options);
		SaplN3Producer producer2 = new SaplN3Producer(model2, model2.prefixer, options);

		//TODO Cannot output all of S-APL document
		String str1 = producer1.produceN3(model1.contexts.get(SaplConstants.GENERAL_CONTEXT));
		String str2 = producer2.produceN3(model2.contexts.get(SaplConstants.GENERAL_CONTEXT));
		
		if(!str1.isEmpty()) str1 += " .";
		if(!str2.isEmpty()) str2 += " .";

		String tmpdir = System.getProperty("java.io.tmpdir");
		String now = String.valueOf(System.currentTimeMillis());
		String filename1 = tmpdir+"sapl.ide/ontology_"+now+"_1.n3";
		String filename2 = tmpdir+"sapl.ide/ontology_"+now+"_2.n3";

		RABUtils.putResult(null, str1, filename1);
		RABUtils.putResult(null, str2, filename2);

		URI source = new URI("file:///"+filename1.replace("\\","/"));
		URI target = new URI("file:///"+filename2.replace("\\","/"));
		
		String eclipseHome = System.getProperty("eclipse.home.location");
		eclipseHome = eclipseHome.substring(eclipseHome.indexOf("file:")+6);
		Bundle b = Platform.getBundle(Activator.PLUGIN_ID);
		String bundleLocation = b.getLocation();
		int ind = bundleLocation.indexOf(eclipseHome);
		if(ind!=-1) bundleLocation = bundleLocation.substring(ind);
		else{
			int ind2 = bundleLocation.indexOf("dropins");
			if(ind2==-1) bundleLocation.indexOf("plugins");
			if(ind2!=-1)
				bundleLocation = eclipseHome + bundleLocation.substring(ind2);
			else bundleLocation = bundleLocation.substring(bundleLocation.indexOf("file:")+6);
		}

		ASE.WORDNET_DICT = bundleLocation+"dict";
		
		Alignment alignment = ASE.align(source, target);
		
		new File(filename1).deleteOnExit();
		new File(filename2).deleteOnExit();
		
		return alignment;
	}

}
