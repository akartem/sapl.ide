package sapl.ide.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.ide.editors.CompletionProcessor;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (19.03.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class RdfClassGenerator {
	
	public static String prefix(String classID){
		String prefixedID = classID;
		for(String ns : CompletionProcessor.namespacesR.keySet()){
			if(classID.startsWith(ns))
				prefixedID = CompletionProcessor.namespacesR.get(ns)+classID.substring(ns.length());
		}
		return prefixedID;
	}
	
	public static String generate(String classID, SaplModel model, Map<String,String> mappedVars){
		
		String cl = prefix(classID);
    	String definition = "?uri a "+cl+" .";
    	
		String query = "?property <"+SaplConstants.RDFS_NS+"domain> ?domain. {?property <"+SaplConstants.RDFS_NS+"range> ?range} <"+SaplConstants.SAPL_NS+"is> <"+SaplConstants.SAPL_NS+"Optional>";
		SaplQueryResultSet rs = new SaplQueryResultSet();
		boolean r = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, model, false, rs);
		if(r && rs.getNumberOfSolutions()>0){
			Map<String,Set<String>> domainsR = new HashMap<String,Set<String>>();
			Map<String,String> ranges = new HashMap<String,String>();
	    	for(int i=0; i<rs.getNumberOfSolutions(); i++){
	    		Map<String, String> sol = rs.getSolution(i);
	    		String prop = sol.get("property");
	    		String domain = sol.get("domain");
	    		String range = sol.get("range");
	    		Set<String> ds = domainsR.get(domain);
	    		if(ds==null){
	    			ds = new HashSet<String>();
	    			domainsR.put(domain, ds);
	    		}
	    		ds.add(prop);
	    		if(range!=null) ranges.put(prop, range);
	    	}
	    	
			String props = generatePropertiesFor(classID, domainsR, ranges, mappedVars, new HashMap<String,Integer>(), new HashSet<String>());
			if(!props.isEmpty()){
				definition = "[a "+cl+"] "+ props+" .";
			}
		}	
		
		return definition;
	}

	private static String generatePropertiesFor(String classID, Map<String, Set<String>> domainsR, Map<String, String> ranges, Map<String,String> mappedVars, Map<String, Integer> definedVars, Set<String> processedClasses) {
		processedClasses.add(classID);
		String result = "";
		String result2 = "";
		Set<String> props = domainsR.get(classID);
		if(props!=null){
			for(String prop : props){
				boolean found = false;
				String prefixed = null;
				String localname = null;
	    		for(String ns : CompletionProcessor.namespacesR.keySet()){
	    			if(prop.startsWith(ns)){
	    				found = true;
	    				localname = prop.substring(ns.length());
	    				prefixed = CompletionProcessor.namespacesR.get(ns)+localname;
	    			}
	    		}
	    		if(!found){
	    			prefixed = "<"+prop+">";
	    			int ind = prop.lastIndexOf("#");
	    			if(ind!=-1) localname = prop.substring(ind+1);
	    			else{
	    				ind = prop.lastIndexOf("/");
		    			if(ind!=-1) localname = prop.substring(ind+1);
	    			}
	    		}
	    		
	    		String varName;
	    		if(mappedVars!=null && mappedVars.containsKey(prop))
	    			varName = mappedVars.get(prop);
	    		else if(localname!=null) varName = localname;
	    		else varName = "var";
	    		
	    		String range = ranges.get(prop);
	    		if(range!=null && !processedClasses.contains(range) && domainsR.get(range)!=null){
	    			String innerProps = generatePropertiesFor(range, domainsR, ranges, mappedVars, definedVars, processedClasses);
	    			result2 += (result2.isEmpty()?"":"; ")+prefixed+" [ "+innerProps+" ]";
	    		}
	    		else {
	    			String var;
		    		Integer index = definedVars.get(varName);
		    		if(index==null){
		    			definedVars.put(varName, 1);
		    			var = SaplConstants.VARIABLE_PREFIX+varName;
		    		}
		    		else{
		    			index++;
		    			definedVars.put(varName, index);
		    			var = SaplConstants.VARIABLE_PREFIX+varName+index;
		    		}
		    		result += (result.isEmpty()?"":"; ")+prefixed+" "+var;
	    		}
			}
			if(!result2.isEmpty())
				result += (result.isEmpty()?"":"; ")+result2;
		}
		return result;
	}

}
