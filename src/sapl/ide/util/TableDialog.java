package sapl.ide.util;

import java.util.List;

import org.eclipse.jface.util.Geometry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (02.11.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class TableDialog extends Dialog{
	
		int selectedRow = -1;
		int[] selectedRows = null;
		int[] preselectedRows = null;
		Shell shell;
		Point location = null;

		Table table;
		
		List<List<String>> tableData;
			
		public TableDialog (Shell parent, int style) {
			super (parent, style);
		}
			
		public void setTableData(List<List<String>> tableData) {
			this.tableData = tableData;
		}
			
		public TableDialog (Shell parent) {
			this (parent, 0); // your default style bits go here (not the Shell's style bits)
		}
			
		public void setLocation(Point point){
			location=point;
		}
		
		public int open () {
			selectedRow = -1;
			showTable(false);
			return selectedRow;
		}
		
		private void showTable(boolean multi){
			Shell parent = getParent();
			shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
			
			shell.setText(getText());

			shell.setLayout(new GridLayout());
			if(!multi)
				table = new Table (shell, SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION);
			else 
				table = new Table (shell, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION);
			table.setLinesVisible (true);
			table.setHeaderVisible (true);
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			data.heightHint = 200;
			table.setLayoutData(data);
			List<String> titles = tableData.get(0);
			for (int i=0; i<titles.size(); i++) {
				TableColumn column = new TableColumn (table, SWT.NONE);
				column.setText (titles.get(i));
			}	
			for (int i=1; i<tableData.size(); i++) {
				List<String> row = tableData.get(i);
				TableItem item = new TableItem (table, SWT.NONE);
				for (int j=0; j<row.size(); j++)
					item.setText (j, row.get(j));
			}
			for (int i=0; i<titles.size(); i++) {
				table.getColumn(i).pack();
			}	
				
			if(!multi)
				table.setSelection(0);
			else {
				table.setSelection(preselectedRows);
			}
			
			Button ok = new Button (shell, SWT.PUSH);
			ok.setText ("Select");
			if(!multi){
				ok.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						selectedRow = table.getSelectionIndex();
						shell.close(); 
					}
				});
			}
			else{
				ok.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						selectedRows = table.getSelectionIndices();
						shell.close(); 
					}
				});
			}
			
			Button cancel = new Button (shell, SWT.PUSH);
			cancel.setText ("Cancel");
			cancel.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					shell.close();
				}
			});				
			shell.setDefaultButton (cancel);
			shell.pack();

			if(location==null){
				Monitor monitor = parent.getMonitor();
				Rectangle monitorBounds = monitor.getClientArea();
				Point centerPoint = Geometry.centerPoint(monitorBounds);
				Point size = shell.getSize();
				location = new Point(centerPoint.x - (size.x / 2), Math.max(
						monitorBounds.y, Math.min(centerPoint.y
								- (size.y * 2 / 3), monitorBounds.y
								+ monitorBounds.height - size.y)));
			}
			shell.setLocation(location);
			
			shell.open();
			Display display = parent.getDisplay();
			while (!shell.isDisposed ()) {
				if (!display.readAndDispatch ()) display.sleep ();
			}
		}
		
		public int[] openMultiSelect(int[] preselected) {
			selectedRows = null; 
			preselectedRows = preselected;
			showTable(true);
			return selectedRows;
		}

}


