package sapl.ide.util;

import org.eclipse.jface.util.Geometry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (18.03.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class InputDialog extends Dialog{
		Shell shell;
		Point location = null;

		Text text;
		String input = null;
		
		public InputDialog (Shell parent, int style) {
			super (parent, style);
		}
			
		public InputDialog (Shell parent) {
			this (parent, 0); // your default style bits go here (not the Shell's style bits)
		}
			
		public void setLocation(Point point){
			location = point;
		}

		public String open () {
			Shell parent = getParent();
			shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
			shell.setText(getText());

			shell.setLayout(new GridLayout());
			
			text = new Text(shell, SWT.BORDER | SWT.H_SCROLL);
			text.append ("                                                                                                                                              \n");
			
			Button ok = new Button (shell, SWT.PUSH);
			ok.setText ("OK");
			ok.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					input = text.getText();
					shell.close();
				}
			});				
			Button cancel = new Button (shell, SWT.PUSH);
			cancel.setText ("Cancel");
			cancel.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					shell.close();
				}
			});				
			shell.setDefaultButton (cancel);
			shell.pack();
			text.setText("");

			if(location==null){
				Monitor monitor = parent.getMonitor();
				Rectangle monitorBounds = monitor.getClientArea();
				Point centerPoint = Geometry.centerPoint(monitorBounds);
				Point size = shell.getSize();
				location = new Point(centerPoint.x - (size.x / 2), Math.max(
						monitorBounds.y, Math.min(centerPoint.y
								- (size.y * 2 / 3), monitorBounds.y
								+ monitorBounds.height - size.y)));
			}
			shell.setLocation(location);

			shell.open();
			Display display = parent.getDisplay();
			while (!shell.isDisposed ()) {
				if (!display.readAndDispatch ()) display.sleep ();
			}
			return input;
		}
}


