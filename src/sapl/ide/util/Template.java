package sapl.ide.util;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (18.03.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class Template implements Comparable<Template>{
	public String name;
	public String template;
	
	public Template(String n, String t){
		name = n;
		template = t;
	}

	@Override
	public int compareTo(Template o) {
		return this.name.compareTo(o.name);
	}

}
