package sapl.ide.editors;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.swt.graphics.Image;

import sapl.core.rab.Parameter;
import sapl.core.SaplConstants;
import sapl.core.sapln3.SaplN3Parser;
import sapl.ide.Activator;
import sapl.ide.util.Template;

/**
 * @author Artem Katasonov (VTT + Elisa since July 2018)
 * 
 * @version 2.1 (07.08.2019)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class CompletionProcessor implements IContentAssistProcessor{ 

	private final IContextInformation[] NO_CONTEXTS = { };
	private final char[] PROPOSAL_ACTIVATION_CHARS = { ':' };
	private final char[] CONTEXT_ACTIVATION_CHARS = { };
	
	static HashMap<String, String> namespaces = new HashMap<String, String>();
	public static HashMap<String, String> namespacesR = new HashMap<String, String>();
	
	static String saplPrefix = "s:";
	static String rdfPrefix = "rdf:";
	static String ontoPrefix = "o:";
	
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int offset) {

		IDocument document = viewer.getDocument();
		getNamespaces(document);
		
		Word prefixWord = getPrefixOfCurrentToken(document, offset);
		String prefix = prefixWord.getWord();

		ArrayList<ICompletionProposal> prs = new ArrayList<ICompletionProposal>();

		saplPrefix = namespacesR.get(SaplConstants.SAPL_NS);
		rdfPrefix = namespacesR.get(SaplConstants.RDF_NS);
		ontoPrefix = namespacesR.get(SaplConstants.ONTONUT_NS);
		String rabPrefix = namespacesR.get(SaplConstants.RAB_NS);
		String paramPrefix = namespacesR.get(SaplConstants.PARAM_NS);
		
		if(saplPrefix!=null && prefix.startsWith(saplPrefix)){
			String endPart = prefix.substring(saplPrefix.length()).toLowerCase();
			for(String keyword : SaplEditor.keywords){
				if(endPart.length()==0 || keyword.toLowerCase().startsWith(endPart)){
					prs.add(new CompletionProposal(keyword, offset-endPart.length(), endPart.length(), keyword.length()));
				}
			}
		}
		if(ontoPrefix!=null && prefix.startsWith(ontoPrefix)){
			String endPart = prefix.substring(ontoPrefix.length()).toLowerCase();
			for(String keyword : SaplEditor.ontoKeywords){
				if(endPart.length()==0 || keyword.toLowerCase().startsWith(endPart)){
					prs.add(new CompletionProposal(keyword, offset-endPart.length(), endPart.length(), keyword.length()));
				}
			}
		}
		else if(rabPrefix!=null && prefix.startsWith(rabPrefix)){
			String endPart = prefix.substring(rabPrefix.length()).toLowerCase();
			for(String keyword : SaplEditor.rabs){
				if(endPart.length()==0 || keyword.toLowerCase().startsWith(endPart.toLowerCase())){
					prs.add(new CompletionProposal(keyword, offset-endPart.length(), endPart.length(), keyword.length()));
				}
			}
		}
		else if(rabPrefix!=null && paramPrefix!=null && prefix.startsWith(paramPrefix)){
			String endPart = prefix.substring(paramPrefix.length()).toLowerCase();
			String rabName = getLastWordStartingWith(document, offset, rabPrefix).getWord();
			rabName = rabName.substring(2);

			try {
				Class<?> cl = Class.forName(rabName);
				Field[] fields = cl.getDeclaredFields();
				for (Field field : fields){
					Parameter paramAnnotation = field.getAnnotation(Parameter.class);
					if(paramAnnotation!=null){
						String paramName = paramAnnotation.name();
						String type = field.getType().getName();
						String text = paramName + " - "+type.substring(type.lastIndexOf('.')+1);
						if(endPart.length()==0 || paramName.toLowerCase().startsWith(endPart)){
							prs.add(new CompletionProposal(paramName, offset-endPart.length(), endPart.length(), paramName.length(), null, text, null, null));
						}
					}
				}

			} catch (ClassNotFoundException e) {
				System.err.println("Cannot find class: "+rabName);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		else if(prefix.startsWith(SaplConstants.VARIABLE_PREFIX)){
			String endPart = prefix.substring(SaplConstants.VARIABLE_PREFIX.length()).toLowerCase();
			List<String> vars = new ArrayList<String>();
			int start = getThisBlockStart(document, prefixWord.getOffset());
			
			try {
				String block = document.get(start, prefixWord.getOffset()-start);
				
				String[] tokens = block.split("[\\s\\{\\}\\[\\]()]+");
				for(String token: tokens){
					if(token.startsWith(SaplConstants.VARIABLE_PREFIX)){
						String varName = token.substring(SaplConstants.VARIABLE_PREFIX.length());
						if(varName.endsWith(".") || varName.endsWith(",") || varName.endsWith(";"))
							varName = varName.substring(0,varName.length()-1);
						if(!vars.contains(varName)) vars.add(varName);
					}
				}
				
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			
			Collections.sort(vars);
			
			for(String var : vars){
				if(endPart.length()==0 || var.toLowerCase().startsWith(endPart))
					prs.add(new CompletionProposal(var, offset-endPart.length(), endPart.length(), var.length(), null, var, null, null));
			}
		}
		else{
			String previous = getNthLastWord(document, offset, 2).getWord();
			if(previous.equals(saplPrefix+"expression") || previous.equals("<-") || previous.equals(">") || previous.equals("<") || previous.equals(">=") || previous.equals("<=") || previous.equals("=") || previous.equals("!=")){
				for(String keyword : SaplEditor.functions.keySet()){
					String prefixLower = prefix.toLowerCase();
					if(keyword.toLowerCase().startsWith(prefixLower)){
						String params = SaplEditor.functions.get(keyword);
						String insert = keyword+params;
						prs.add(new CompletionProposal(insert, offset-prefix.length(), prefix.length(), keyword.length()+1));
					}
				}
			}
			else if(!prefix.isEmpty()){
				for(String keyword : SaplEditor.shorthands){
					if(keyword.startsWith(prefix)){
						prs.add(new CompletionProposal(keyword, offset-prefix.length(), prefix.length(), keyword.length()));
					}
				}
			}
			else {
				Image img = Activator.getDefault().getImageRegistry().get(Activator.ICON_TEMPLATE);
				String indent = getLastIndent(document, offset);
				
				if(ontoPrefix!=null && getNthLastWord(document, offset, 2).getWord().equals(ontoPrefix+"semantics")){
					
					Word w = getLastWordStartingWith(document, offset, ontoPrefix+"Ontonut");
					if(w!=null){
						Word idword = getNthLastWord(document, w.getOffset()-1, 2);
						if(idword!=null){
							String id = idword.getWord();
							if(!id.startsWith("<")){
								int nsind = id.indexOf(':');
								if(nsind!=-1){
									String localname = id.substring(nsind+1);
									if(nsind==0){
										id = SaplConstants.EMPTY_NS+localname;
									}
									else{
										String pref = id.substring(0,nsind+1);
										String namespace = namespaces.get(pref);
										if(namespace!=null)
											id = namespace+localname;
									}
								}
							}
							Word wtype = getLastWordStartingWith(document, offset, ontoPrefix+"type");
							if(wtype!=null){
								Word wclass = getNextWord(document, wtype.getOffset()+wtype.getWord().length());
								String ontonutType = wclass.getWord();
								int plus = ontonutType.endsWith(".")? 1:0;
								if(ontonutType.startsWith("\"") || ontonutType.startsWith("'"))
									ontonutType = ontonutType.substring(1,ontonutType.length()-1-plus);

								Word wuri = getLastWordStartingWith(document, offset, ontoPrefix+"uri");
								String ontonutUri = null;
								if(wuri!=null){
									Word waddr = getNextWord(document, wuri.getOffset()+wuri.getWord().length());
									ontonutUri = waddr.getWord();
									plus = ontonutUri.endsWith(".")? 1:0;
									if(ontonutUri.startsWith("\"") || ontonutUri.startsWith("'"))
										ontonutUri = ontonutUri.substring(1,ontonutUri.length()-1-plus);
								}
	
								prs.add(new OntonutSyntaxProposal(id, ontonutType, ontonutUri, indent, offset, 0, 0, img, "Autofill ontonut syntax", null, null));
							}
						}
					}
					
				}
				else{
					prs.add(new RdfsClassGenerationProposal(indent, offset, 0, 0, img, "Generate a class instance", null, null));
					prs.add(new OntonutGenerationProposal(indent, offset, 0, 0, img, "Generate a simple HTTP ontonut", null, null));

					for(Template t : SaplEditor.templates){
						if(t.name.endsWith(".sapl")) continue;

						String template = t.template;
						template = template.replace("\n", "\n"+indent);
						
					    int cursor = 0;
						int ind = template.indexOf("$cursor$");
					    if(ind!=-1){
					    	cursor = ind;
					    	template = template.replace("$cursor$", "");
					    }
						
						prs.add(new CompletionProposal(template, offset, 0, cursor, img, t.name, null, null));
					}
				}			
			}
			
		}

		ICompletionProposal[] proposals = (ICompletionProposal[]) prs.toArray(new ICompletionProposal[prs.size()]);
		return proposals;

	}

	private void getNamespaces(IDocument document) {
		namespaces.clear();
		namespacesR.clear();
		String text = document.get();
		int ind=0;
		while((ind = text.indexOf(SaplN3Parser.PREFIX_KEYWORD,ind))!=-1){
			ind = ind+SaplN3Parser.PREFIX_KEYWORD.length()+1;
			Word w = getNextWord(document, ind);
			if(w!=null){
				Word w2 = getNextWord(document, w.getOffset()+w.getWord().length());
				if(w2!=null){
					String prefix = w.getWord();
					String namespace = w2.getWord();
					int plus = namespace.endsWith(".")? 1:0; 
					namespace = namespace.substring(1,namespace.length()-1-plus);
					namespaces.put(prefix, namespace);
					namespacesR.put(namespace, prefix);
				}
			}
		}
	}

	private Word getPrefixOfCurrentToken(IDocument doc, int offset) {
		ExpressionPartDetector wd = new ExpressionPartDetector();
		try {
			for (int pos = offset-1; pos >= 0; pos--) {
				char c = doc.getChar(pos);
				if (!wd.isWordPart(c))
					return new Word(doc.get(pos + 1, offset-pos-1), pos+1);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return new Word("",0);
	}

	private Word getNthLastWord(IDocument doc, int offset, int nth) {
		UriOrStringDetector wd = new UriOrStringDetector();
		try {
			for (int n = 1; n <= nth; n++){
				int pos;
				for (pos = offset-1; pos >= 0; pos--) {
					char c = doc.getChar(pos);
					if (!wd.isWordPart(c) && pos < offset-1)
						if(n==nth)
							return new Word(doc.get(pos + 1, offset-pos-1), pos+1);
						else{
							offset = pos;
							break; // continue to next word
						}
				}
				for (pos = offset-1; pos >= 0 && !wd.isWordPart(doc.getChar(pos)); pos--);
				offset = pos+1;
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Word getLastWordStartingWith(IDocument doc, int offset, String prefix) {
		int end = offset;
		UriOrStringDetector wd = new UriOrStringDetector();
		try {
			for (int pos = offset-1; pos >= 0; pos--) {
				char c = doc.getChar(pos);
				if (!wd.isWordPart(c)){
					String word = doc.get(pos + 1, end-pos-1);
					if(word.startsWith(prefix))
						return new Word(word,pos+1);
					else end = pos;
				}
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Word getNextWord(IDocument doc, int offset) {
		UriOrStringDetector wd = new UriOrStringDetector();
		try {
			int pos;
			for (pos = offset; pos < doc.getLength() && !wd.isWordPart(doc.getChar(pos)); pos++);
			offset = pos;
			for (pos = offset; pos < doc.getLength(); pos++) {
				char c = doc.getChar(pos);
				if (!wd.isWordPart(c))
					return new Word(doc.get(offset, pos-offset), offset);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return new Word("",0);
	}

	private String getLastIndent(IDocument doc, int offset) {
		try {
			int start = offset-1; 
			while (start >= 0 && doc.getChar(start)!= '\n') start--;
			int end = start;
			while (end < offset && Character.isWhitespace(doc.getChar(end))) end++;
			
			return doc.get(start+1, end-start-1);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	private int getThisBlockStart(IDocument doc, int end){
		String text = doc.get();
		int ind=0;
		int openCounter = 0;
		int lastBlockStart = 0;
		int lastArrow = -1;
		
		while(true){
			int nextOpen = text.indexOf("{", ind);
			int nextClose = text.indexOf("}", ind);
			if(nextOpen==-1 && nextClose==-1) return lastBlockStart;
			
			if(lastArrow<ind){
				int nextArrow1 = text.indexOf("=>", ind); if(nextArrow1==-1) nextArrow1=Integer.MAX_VALUE;
				int nextArrow2 = text.indexOf("->", ind); if(nextArrow2==-1) nextArrow2=Integer.MAX_VALUE;
				int nextArrow3 = text.indexOf(">>", ind); if(nextArrow3==-1) nextArrow3=Integer.MAX_VALUE;
				int nextArrow = Math.min(nextArrow1, Math.min(nextArrow2, nextArrow3));
				if(nextArrow<Integer.MAX_VALUE) lastArrow = nextArrow;
			}
			
			if(nextOpen!=-1 && (nextClose==-1 || nextOpen<nextClose)){
				if(nextOpen > end) return lastBlockStart;
				if(openCounter==0){
					if(lastArrow<nextOpen && text.substring(lastArrow+2, nextOpen).trim().isEmpty()) ;
					else lastBlockStart = nextOpen;
				}
				ind = nextOpen+1;
				openCounter++;
			}
			else{
				if(nextClose > end) return lastBlockStart;
				ind = nextClose+1;
				if(openCounter>0) openCounter--;
			}
		}
		
	} 
	

	@Override
	public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) { 
		return NO_CONTEXTS;
	}

	@Override
	public char[] getCompletionProposalAutoActivationCharacters() {
		return PROPOSAL_ACTIVATION_CHARS;
	}

	@Override
	public char[] getContextInformationAutoActivationCharacters() {
		return CONTEXT_ACTIVATION_CHARS;
	}
	@Override
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	@Override
	public String getErrorMessage() {
		return null;
	}
	
	
	private class Word {
		String word;
		int offset;
		
		Word(String w, int o){
			word = w;
			offset = o;
		}
		
		String getWord(){
			return word;
		}
		
		int getOffset(){
			return offset;
		}
	}

}