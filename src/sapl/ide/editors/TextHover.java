package sapl.ide.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.swt.graphics.Point;

import sapl.core.SaplConstants;
import sapl.core.sapln3.SaplN3Parser;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (22.05.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class TextHover implements ITextHover{

	@Override
	public String getHoverInfo(ITextViewer textViewer, IRegion region) {
    	IDocument document = textViewer.getDocument();
    	String word = getCurrentWord(document, region.getOffset());
    	if(CompletionProcessor.saplPrefix!=null && word.startsWith(CompletionProcessor.saplPrefix))
    		word = SaplConstants.SAPL_NS+word.substring(CompletionProcessor.saplPrefix.length());
    	else if(CompletionProcessor.rdfPrefix!=null && word.startsWith(CompletionProcessor.rdfPrefix))
    		word=SaplConstants.RDF_NS+word.substring(CompletionProcessor.rdfPrefix.length());
    	else if(CompletionProcessor.ontoPrefix!=null && word.startsWith(CompletionProcessor.ontoPrefix))
    		word=SaplConstants.ONTONUT_NS+word.substring(CompletionProcessor.ontoPrefix.length());
    	String help = SaplEditor.help.get(word);
    	if(help==null){
    		String replace = SaplN3Parser.replace.get(word);
    		if(replace!=null){
    			word = replace;
    			help = SaplEditor.help.get(replace);
    		}
    	}
    	if(help==null){
    		String args = SaplEditor.functions.get(word);
    		if(args!=null){
    			help = args+"\n\n"+SaplEditor.help.get(SaplConstants.FUNC_NS+word);
    		}
    	}
    	if(help!=null){
    		help = word+"\n\n"+help; 
    		return help;
    	}
    	return word;
	}

	@Override
	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		Point selection = textViewer.getSelectedRange();
		if (selection.x <= offset && offset < selection.x + selection.y) {
			return new Region(selection.x, selection.y);
		}
		return new Region(offset, 0);
	}
	
	private String getCurrentWord(IDocument doc, int offset) {
		ExpressionPartDetector wd = new ExpressionPartDetector();
		int start=0, end=doc.getLength();
		try {
			for (int n = offset; n >= 0; n--) {
				char c = doc.getChar(n);
				if (!wd.isWordPart(c)){
					start = n+1;
					break;
				}
			}
			for (int n = offset; n < doc.getLength(); n++) {
				char c = doc.getChar(n);
				if (!wd.isWordPart(c)){
					end = n;
					break;
				}
			}
			if(start<end){
				String word = doc.get(start, end-start);
				if(word.endsWith(".")) word = word.substring(0,word.length()-1);
				return word;
			}
			else return "";
			
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return "";
	}

}
