package sapl.ide.editors;

import java.util.ArrayList;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordPatternRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;

import sapl.core.SaplConstants;
import sapl.core.sapln3.SaplN3Parser;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (09.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class DocumentScanner extends RuleBasedScanner {

	public DocumentScanner(ColorManager manager) {
		IToken keyword = new Token(new TextAttribute(manager.getColor(ColorConstants.KEYWORD), null, SWT.BOLD));
		IToken def = new Token(new TextAttribute(manager.getColor(ColorConstants.DEFAULT)));
		IToken var = new Token(new TextAttribute(manager.getColor(ColorConstants.VARIABLE)));

		ArrayList<IRule> rules = new ArrayList<IRule>();

		//Skip whitespaces
		rules.add(new WhitespaceRule(new WhitespaceDetector()));

		//Variables 
		WordPatternRule wpr = new FixedWordPatternRule(new WordDetector(), SaplConstants.VARIABLE_PREFIX, "", var);
		rules.add(wpr);

		//S-APL keywords
		WordRule wr = new WordRule(new WordDetector(), def); 
		rules.add(wr);
		wr.addWord(SaplN3Parser.PREFIX_KEYWORD, keyword);
		wr.addWord(SaplConstants.ANYTHING, var);
		for(String key : SaplEditor.keywords)
			wr.addWord(key, keyword);
		for(String key : SaplEditor.shorthands)
			wr.addWord(key, keyword);
		

		setRules(rules.toArray(new IRule[rules.size()]));
	}
}
