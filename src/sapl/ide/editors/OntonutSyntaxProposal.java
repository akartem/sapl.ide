package sapl.ide.editors;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension4;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplActionEngine;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.ide.Activator;
import sapl.shared.eii.HttpOntonut;
import sapl.shared.eii.Ontonut;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.3 (18.01.2017)
 * 
 * Copyright (C) 2014-2017, VTT
 * 
 * */

public class OntonutSyntaxProposal extends ExtensibleCompletionProposal implements ICompletionProposalExtension4{

	String ontonutId;
	String ontonutClass;
	String ontonutUri;
	String lastIndent;

	public OntonutSyntaxProposal(String ontonutId, String ontonutClass, String ontonutUri, String lastIndent,
			int replacementOffset, int replacementLength, int cursorPosition,
			Image image, String displayString,
			IContextInformation contextInformation,
			String additionalProposalInfo) {
		super("", replacementOffset, replacementLength, cursorPosition,
				image, displayString, contextInformation, additionalProposalInfo);

		this.ontonutClass = ontonutClass;
		this.ontonutUri = ontonutUri;
		this.ontonutId = ontonutId;
		this.lastIndent = lastIndent;
	}

	@Override
	public void apply(IDocument document) {
		fReplacementString = produceReplacementString(document);
		if(fReplacementString!=null){
			try {
				document.replace(fReplacementOffset, fReplacementLength, fReplacementString);
			} catch (BadLocationException x) {
			}
		}
	}


	private String produceReplacementString(IDocument document) {

		try {
			Ontonut ontonut;
			try {
				@SuppressWarnings("unchecked")
				Class<Ontonut> cl = (Class<Ontonut>) Class.forName(this.ontonutClass);
				ontonut = cl.newInstance();
			}
			catch(Exception e){
				String message = "The class "+this.ontonutClass+" could not be found!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}
			
			String content = document.get();
			SaplCode saplCode;
			try {
				saplCode = SaplN3Parser.compile(content,"document");
			}
			catch (Throwable e){
				String message = "The file has syntax errors: cannot proceed!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}
			
			if(this.ontonutUri!=null && ontonut instanceof HttpOntonut){
				String extendedUri = getExtendedUri(this.ontonutUri);
				if(extendedUri==null) return null;
				
				if(!extendedUri.equals(this.ontonutUri)){
					List<String> code = saplCode.getCode();
					int ind = code.indexOf(this.ontonutUri);
					if(ind==-1) ind = code.indexOf("\""+this.ontonutUri+"\"");
					if(ind!=-1){
						code.set(ind, "\""+extendedUri+"\"");
					}
				}
			}
			
			SaplModel model = new SaplModel(false, false);
			SaplN3Parser.load(saplCode.getCode(), model, SaplConstants.GENERAL_CONTEXT, null, "document", false);

			
			ReusableAtomicBehavior rab = new ReusableAtomicBehavior(){
				protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {}
				protected void doAction() throws Exception {}
			};
			rab.setSaplActionEngine(new SaplActionEngine(model, null));
			ontonut.configure(this.ontonutId, 1, rab, null);
			
			boolean ok = ontonut.loadDefinition();
			if(!ok){
				String message = "The ontonut specification is now well formed!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, message));
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}

			try{
				String definition = ontonut.generateSyntaxDefinition(null,null);
				definition = definition.replace("\n", "\n"+this.lastIndent);
				return definition; 
			}
			catch (Throwable e){
				String message = "The syntax defintion creation failed! See the Error Log view.";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}
		} catch (Exception e) {
			String message = "An exception occured! See the Error Log view.";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			MessageDialog.openInformation(new Shell(),"S-APL IDE",message);			
		}
		return null;
	}
	
	public static String getExtendedUri(String uri){
		String extendedUri = null;
		if(uri!=null){
			URL url = null;
			try {
				url = new URL(uri);
				return uri;
			} catch (MalformedURLException e) {}
			if(url==null){
				File file = new File(uri);
				if(!file.exists()){
					IEditorInput input = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getEditorInput();
					if (input instanceof IFileEditorInput){
						String projectRoot = ((IFileEditorInput)input).getFile().getProject().getLocation().toString();
						extendedUri = uri;
						if(extendedUri.startsWith("./")) extendedUri = extendedUri.substring(2);
						extendedUri = projectRoot+"/"+extendedUri;
						file = new File(extendedUri);
						if(!file.exists()){
							String message = uri+" could not be found!";
							MessageDialog.openInformation(new Shell(),"S-APL IDE",message);			
							return null;
						}
						else extendedUri = "file:///"+extendedUri;
					}
				}
			}
		}
		return extendedUri;
	}

	@Override
	public boolean isAutoInsertable() {
		return false;
	}	
}
