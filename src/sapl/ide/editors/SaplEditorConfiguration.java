package sapl.ide.editors;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.DefaultAnnotationHover;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (09.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class SaplEditorConfiguration extends SourceViewerConfiguration {
	private DoubleClickStrategy doubleClickStrategy;
	private ColorManager colorManager;

	private TextHover textHover;
	private DocumentScanner docScanner;

	public SaplEditorConfiguration(ColorManager colorManager) {
		this.colorManager = colorManager;
	}

	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] {
				IDocument.DEFAULT_CONTENT_TYPE,
				PartitionScanner.SAPL_MULTILINE_STRING
		};
	}

	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(
			ISourceViewer sourceViewer,
			String contentType) {
		if (doubleClickStrategy == null)
			doubleClickStrategy = new DoubleClickStrategy();
		return doubleClickStrategy;
	}

	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {

		PresentationReconciler reconciler = new PresentationReconciler();

		DefaultDamagerRepairer dr =
				new DefaultDamagerRepairer(getDocumentScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		NonRuleBasedDamagerRepairer ndr =
				new NonRuleBasedDamagerRepairer(
						new TextAttribute(
								colorManager.getColor(ColorConstants.COMMENT)));
		reconciler.setDamager(ndr, PartitionScanner.SAPL_MULTILINE_COMMENT);
		reconciler.setRepairer(ndr, PartitionScanner.SAPL_MULTILINE_COMMENT);

		NonRuleBasedDamagerRepairer ndr2 =
				new NonRuleBasedDamagerRepairer(
						new TextAttribute(
								colorManager.getColor(ColorConstants.STRING)));
		reconciler.setDamager(ndr2, PartitionScanner.SAPL_MULTILINE_STRING);
		reconciler.setRepairer(ndr2, PartitionScanner.SAPL_MULTILINE_STRING);

		NonRuleBasedDamagerRepairer ndr3 =
				new NonRuleBasedDamagerRepairer(
						new TextAttribute(
								colorManager.getColor(ColorConstants.COMMENT)));
		reconciler.setDamager(ndr3, PartitionScanner.SAPL_SINGLELINE_COMMENT);
		reconciler.setRepairer(ndr3, PartitionScanner.SAPL_SINGLELINE_COMMENT);

		NonRuleBasedDamagerRepairer ndr4 =
				new NonRuleBasedDamagerRepairer(
						new TextAttribute(
								colorManager.getColor(ColorConstants.URI)));
		reconciler.setDamager(ndr4, PartitionScanner.SAPL_URI);
		reconciler.setRepairer(ndr4, PartitionScanner.SAPL_URI);

		return reconciler;
	}

	@Override
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
		ContentAssistant ca = new ContentAssistant();
		IContentAssistProcessor cap = new CompletionProcessor();
		ca.setContentAssistProcessor(cap, IDocument.DEFAULT_CONTENT_TYPE);
		ca.setContentAssistProcessor(cap, PartitionScanner.SAPL_MULTILINE_STRING);

		ca.setInformationControlCreator(getInformationControlCreator(sourceViewer));
		ca.enableAutoActivation(true);
		ca.enableAutoInsert(true);
		return ca;
	}

	@Override
	public ITextHover getTextHover(ISourceViewer sv, String contentType) {
		if (textHover == null) {
			textHover = new TextHover();
		}
		return textHover;
	}	

	@Override
	public IAnnotationHover getAnnotationHover(ISourceViewer sv) {
		return new DefaultAnnotationHover();
	}

	protected DocumentScanner getDocumentScanner() {
		if (docScanner == null) {
			docScanner = new DocumentScanner(colorManager);
			docScanner.setDefaultReturnToken(
					new Token(
							new TextAttribute(
									colorManager.getColor(ColorConstants.DEFAULT))));
		}
		return docScanner;
	}

}