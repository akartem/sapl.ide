package sapl.ide.editors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension4;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.Cell;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplActionEngine;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.ide.Activator;
import sapl.ide.om.ASEMatcher;
import sapl.ide.om.OntologyMatcher;
import sapl.ide.util.InputDialog;
import sapl.ide.util.ProgressBarDialog;
import sapl.ide.util.RdfClassGenerator;
import sapl.ide.util.TableDialog;
import sapl.ide.util.Template;
import sapl.shared.eii.CsvOntonut;
import sapl.shared.eii.ExcelOntonut;
import sapl.shared.eii.JsonOntonut;
import sapl.shared.eii.Ontonut;
import sapl.shared.eii.XmlOntonut;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.6 (07.02.2018)
 * 
 * Copyright (C) 2014-2018, VTT
 * 
 * */

public class OntonutGenerationProposal  extends ExtensibleCompletionProposal implements ICompletionProposalExtension4 {

	String lastIndent;

	public OntonutGenerationProposal(String lastIndent,
			int replacementOffset, int replacementLength, int cursorPosition,
			Image image, String displayString,
			IContextInformation contextInformation,
			String additionalProposalInfo) {
		super("", replacementOffset, replacementLength, cursorPosition,
				image, displayString, contextInformation, additionalProposalInfo);

		this.lastIndent = lastIndent;
	}

	@Override
	public void apply(final IDocument document) {
		InputDialog dialog = new InputDialog(new Shell());
		dialog.setText("Provide data source URL");
		String url = dialog.open();
		if(url!=null){
			final String extendedUri = OntonutSyntaxProposal.getExtendedUri(url.trim());
			if(extendedUri==null) return;
			
			Shell shell = new Shell();
			final ProgressBarDialog pbDialog = new ProgressBarDialog(shell);
			pbDialog.setText("Working..");
			final ProgressBar pb = pbDialog.open();
			Thread th = new Thread(new Runnable() {
				public void run(){
					fReplacementString = produceReplacementStringForURL(document, extendedUri, pb);
					Display.getDefault().asyncExec(new Runnable() {
						public void run() {
							if(fReplacementString!=null){
								try {
									document.replace(fReplacementOffset, fReplacementLength, fReplacementString);
								} catch (BadLocationException x) {
								}
							}
						}
					});
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Display.getDefault().asyncExec(new Runnable() {
						public void run() {
							pbDialog.close();
						}
					});
				}
			});
			th.start();
		}
	}

	protected String produceReplacementStringForURL(IDocument document, String url, final ProgressBar pb){
		//Load the content of the current document
		String content = document.get();
		SaplCode saplCode;
		try {
			saplCode = SaplN3Parser.compile(content,"document");
		}
		catch (Throwable e){
			String message = "The file has syntax errors: cannot proceed!";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			openInformation(message);		
			return null;
		}
		SaplModel docModel = new SaplModel(false, false);
		SaplN3Parser.load(saplCode.getCode(), docModel, SaplConstants.GENERAL_CONTEXT, null, "document", false);

		//Load remote data
		String result = "";
		for(Template t : SaplEditor.templates){
			if(t.name.equals("ontonut.sapl")) result = t.template;
		}

		result = result.replace("%%uri%%", url);

		String id = url.substring(url.lastIndexOf("/")+1);
		if(id.contains("?")) id = id.substring(0, id.indexOf("?"));
		result = result.replace("%%id%%", ":"+id);

		//Engage the ontonut to produce the ontology representing remote data
		Ontonut ontonut = null;
		try {
			if(url.endsWith(".xls") || url.endsWith(".xlsx")){
				ontonut = new ExcelOntonut();
			}
			else{
				String url_content = HttpTools.getContent(url, HttpOptions.defaultOptions()).trim();
				if(url_content.startsWith("<")){
					ontonut = new XmlOntonut();
				}
				else if(url_content.startsWith("{") || url_content.startsWith("[")){
					ontonut = new JsonOntonut();
				}
				else{
					String rows[] = url_content.split("\n");
					boolean ok = true;
					int curr = -1;
					for(String row : rows){
						int l = row.split(",").length;
						if(curr==-1) curr = l;
						else if(curr!=l){
							ok=false;
							break;
						}
					}
					if(ok) ontonut = new CsvOntonut();
				}
			}
			
			if(ontonut==null){
				String message = "Unknown content type at URL '"+url+"'!";
				openInformation(message);		
				return null;
			}
			
			String type = ontonut.getClass().getName();
			result = result.replace("%%type%%", type);

		} catch (Exception e) {
			String message = "Failed to access URL '"+url+"'!";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			openInformation(message);		
			return null;
		}

		try {
			String str = SaplN3Parser.PREFIX_KEYWORD+" o: <"+SaplConstants.ONTONUT_NS+">.\n"+result.replace("%%"," X ");
			saplCode = SaplN3Parser.compile(str,"document");
		}
		catch (Throwable e){
			String message = "The file has syntax errors: cannot proceed!";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			openInformation(message);		
			return null;
		}

		SaplModel ontonutModel = new SaplModel(false, false);
		SaplN3Parser.load(saplCode.getCode(), ontonutModel, SaplConstants.GENERAL_CONTEXT, null, "document", false);

		ReusableAtomicBehavior rab = new ReusableAtomicBehavior(){
			protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {}
			protected void doAction() throws Exception {}
		};
		rab.setSaplActionEngine(new SaplActionEngine(ontonutModel, null));
		ontonut.configure("#"+id, 1, rab, null);

		boolean ok = ontonut.loadDefinition();
		if(!ok){
			String message = "The ontonut specification is now well formed!";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, message));
			openInformation(message);		
			return null;
		}
		
		//Any ontology in the document to try align to?
		String query = "?x a <"+SaplConstants.OWL_NS+"Ontology>";
		SaplQueryResultSet rs = new SaplQueryResultSet(); 
		boolean hasOntology = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, docModel, false, rs);
		
		String ontology = null;
		if(!hasOntology){
			String message = "The file does not have any instance of owl:Ontology, the alignment step is skipped";
			openInformation(message);		
		}
		else {
			try {
				ontology = ontonut.generateOntologyDefinition();
			} catch (Throwable e) {
				String message = "Failed to process URL '"+url+"' content!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				openInformation(message);		
				return null;
			}
		}
		
		Alignment alignment = null;
		if(ontology!=null){
			saplCode = SaplN3Parser.compile(ontology,"ontology");
			SaplModel dsModel = new SaplModel(false, false);
			SaplN3Parser.load(saplCode.getCode(), dsModel, SaplConstants.GENERAL_CONTEXT, null, "ontology", false);
	
			pb.getDisplay().asyncExec(new Runnable() {
				public void run() {
					if (pb.isDisposed()) return;
					pb.setSelection(10);
				}
			});
	
			//Engage the ontology matcher
			
			try {
				OntologyMatcher matcher = new ASEMatcher();
				alignment = matcher.match(docModel, dsModel);
			} catch (Exception e) {
				String message = "Exception when executing the ontology matcher!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				openInformation(message);		
			}
	
			if(pb.isDisposed()) return null;
			pb.getDisplay().asyncExec(new Runnable() {
				public void run() {
					if (pb.isDisposed()) return;
					pb.setSelection(80);
				}
			});
		}

		//		Set<String> include = new HashSet<String>();
		//		include.add(map.classesMap.keySet());
		//		include.addAll(map.propertiesMap.keySet());

		//Generate syntax definition

		Map<String,String> vars = new HashMap<String,String>();
		try {
			//String syntax = ontonut.generateSyntaxDefinition(include);

			String syntax = ontonut.generateSyntaxDefinition(null, vars);
			
			int ind = result.indexOf("%%syntax%%");
			int ind2 = result.lastIndexOf("\n",ind);
			String syntax_indent = result.substring(ind2+1, ind); 
			syntax = syntax.replace("\n", "\n"+this.lastIndent+syntax_indent);
			result = result.replace("%%syntax%%", syntax);
		} catch (Throwable e) {
			String message = "Failed to process URL '"+url+"' content!";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			openInformation(message);		
			return null;
		}

		String classDef = "";
		String pattern = "";

		Set<String> allDocClasses = findClasses(docModel);

		Set<String> allClassesMapped = null;
		Map<String,Set<String>> classesMappedProps = new HashMap<String,Set<String>>();

		Map<String,String> semVars = new HashMap<String,String>(); 
		Map<String,Double> semVarConfidence = new HashMap<String,Double>(); 

		if(alignment != null && alignment.iterator().hasNext()){
			allClassesMapped = new HashSet<String>();

			Map<String,Set<String>> props = findProperties(docModel);

			for(Cell cell : alignment){
				String obj1 = cell.getObject1().toString();
				if(obj1.startsWith("<") && obj1.endsWith(">"))
					obj1 = obj1.substring(1, obj1.length()-1);
				if(allDocClasses.contains(obj1))
					allClassesMapped.add(obj1);
				else{
					Set<String> domains = props.get(obj1);
					if(domains!=null && !domains.isEmpty()){
						for(String domain: domains){
							if(allDocClasses.contains(domain)){
								allClassesMapped.add(domain);
								Set<String> currMappedProps = classesMappedProps.get(domain);
								if(currMappedProps==null){
									currMappedProps = new HashSet<String>();
									classesMappedProps.put(domain, currMappedProps);
								}
								currMappedProps.add(obj1);
							}
						}
					}

					String obj2 = cell.getObject2().toString();
					if(obj2.startsWith("<") && obj2.endsWith(">"))
						obj2 = obj2.substring(1, obj2.length()-1);						
					String var = vars.get(obj2);
					if(var!=null){
						double conf = cell.getStrength();
						Double currentVarConfidence = semVarConfidence.get(obj1);
						if(currentVarConfidence==null || currentVarConfidence < conf){
							semVars.put(obj1, var);
							semVarConfidence.put(obj1, conf);
						}
					}
				}
			}
		}

		if(allClassesMapped==null || allClassesMapped.isEmpty()){
			allClassesMapped = allDocClasses;
		}

		//Select class for semantics
		if(!allClassesMapped.isEmpty()){
			
			Map<String, Set<String>> innerClasses = findInnerClasses(docModel);
			for(String inner : innerClasses.keySet()){
				this.addToOutherRecursive(inner, innerClasses, classesMappedProps, new HashSet<String>());
			}
			
			final ArrayList<String> selectedClass = new ArrayList<String>(1);
			if(allClassesMapped.size()==1)
				selectedClass.add(allClassesMapped.iterator().next());
			else{
				Map<String,String> classes = new HashMap<String,String>();

				final List<List<String>> table = new ArrayList<List<String>>();
				List<String> header = new ArrayList<String>();
				header.add("Class");
				header.add("URI");
				header.add("Mappings");
				table.add(header);

				Vector<String> classNames = new Vector<String>();
				Vector<Integer> classMappings = new Vector<Integer>();
				for(String cl : allClassesMapped){
					String prefixedID = RdfClassGenerator.prefix(cl);
					classes.put(prefixedID, cl);
					
					Set<String> mappings = classesMappedProps.get(cl);
					int n = (mappings==null)?0:mappings.size();

					boolean inserted = false;
					for(int i=0; i<classMappings.size(); i++){
						if(classMappings.get(i)<n){
							classMappings.insertElementAt(n, i);
							classNames.insertElementAt(prefixedID, i);
							inserted=true;
							break;
						}		
					}
					if(!inserted){
						classMappings.add(n);
						classNames.add(prefixedID);
					}
				}

				for(int i=0; i<classNames.size(); i++){
					String classID = classNames.get(i);
					List<String> row = new ArrayList<String>(); 
					row.add(classID);
					String clName = classes.get(classID);
					row.add(clName);
					int mappings = classMappings.get(i);
					row.add(String.valueOf(mappings));
					table.add(row);
				}
				
				Display.getDefault().syncExec(new Runnable() {
					public void run() {
						TableDialog dialog2 = new TableDialog(new Shell());
						dialog2.setText("Select a class to map to");
						dialog2.setTableData(table);
						int selectedItem = dialog2.open();
						if(selectedItem!=-1)
							selectedClass.add(table.get(selectedItem+1).get(1));
					}
				});
				
			}
		
			//Generate semantics
			if(!selectedClass.isEmpty()){
				String selClass = selectedClass.get(0);
				classDef = RdfClassGenerator.generate(selClass, docModel, semVars);
				int ind = result.indexOf("%%semantics%%");
				int ind2 = result.lastIndexOf("\n",ind);
				String semantics_indent = result.substring(ind2+1, ind); 
				classDef = classDef.replace("\n", "\n"+this.lastIndent+semantics_indent);

				pattern = "* a "+RdfClassGenerator.prefix(selClass);
			}
		}

		result = result.replace("%%semantics%%", classDef);
		result = result.replace("%%pattern%%", pattern);

		pb.getDisplay().asyncExec(new Runnable() {
			public void run() {
				if (pb.isDisposed()) return;
				pb.setSelection(100);
			}
		});		

		return result;
	}
	
	
	private void addToOutherRecursive(String inner, Map<String, Set<String>> innerClasses, Map<String, Set<String>> classesMappedProps, Set<String> visited) {
		Set<String> innerProps = classesMappedProps.get(inner);
		if(innerProps!=null){
			visited.add(inner);
			Set<String> outherClasses  = innerClasses.get(inner);
			if(outherClasses!=null){
				for(String outher : outherClasses){
					if(visited.contains(outher)) continue;
					
					Set<String> outherProps = classesMappedProps.get(outher);
					if(outherProps==null){
						outherProps = new HashSet<String>();
						classesMappedProps.put(outher, outherProps);
					}
					outherProps.addAll(innerProps);
					
					addToOutherRecursive(outher, innerClasses, classesMappedProps, visited);
				}
			}
		}
	}

	protected void openInformation(final String message){
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);				
			}
		});
	}

	protected Set<String> findClasses(SaplModel model){
		String query = "{?cl a <"+SaplConstants.RDFS_NS+"Class>} <"+SaplConstants.SAPL_NS+"or> {?cl a <"+SaplConstants.OWL_NS+"Class>}";
		SaplQueryResultSet rs = new SaplQueryResultSet();
		boolean r = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, model, false, rs);
		if(!r) return new HashSet<String>(1);
		Set<String> result = new HashSet<String>(rs.getNumberOfSolutions());
		for(int i=0; i<rs.getNumberOfSolutions(); i++){
			Map<String, String> vars = rs.getSolution(i);
			String cl = vars.get("cl");
			result.add(cl);
		}
		return result;
	}

	protected Map<String,Set<String>> findProperties(SaplModel model){
		String query = "{{?prop a <"+SaplConstants.RDF_NS+"Property>} <"+SaplConstants.SAPL_NS+"or> {?prop a <"+SaplConstants.OWL_NS+"ObjectProperty>}} <"+SaplConstants.SAPL_NS+"or> {?prop a <"+SaplConstants.OWL_NS+"DatatypeProperty>}";
		query += " . ?prop <"+SaplConstants.RDFS_NS+"domain> ?domain";
		SaplQueryResultSet rs = new SaplQueryResultSet();
		boolean r = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, model, false, rs);
		if(!r) return new HashMap<String,Set<String>>(1);
		Map<String,Set<String>> result = new HashMap<String,Set<String>>(rs.getNumberOfSolutions());
		for(int i=0; i<rs.getNumberOfSolutions(); i++){
			Map<String, String> vars = rs.getSolution(i);
			String property = vars.get("prop");
			String domain = vars.get("domain");
			Set<String> domains = result.get(property);
			if(domains==null){
				domains = new HashSet<String>(1);
				result.put(property, domains);
			}
			
			domains.add(domain);
		}
		return result;
	}
	
	protected Map<String,Set<String>> findInnerClasses(SaplModel model){
		String query = "?prop <"+SaplConstants.RDFS_NS+"domain> ?domain; <"+SaplConstants.RDFS_NS+"range> ?range";
		SaplQueryResultSet rs = new SaplQueryResultSet();
		boolean r = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, model, false, rs);
		if(!r) return new HashMap<String,Set<String>>(1);
		Map<String,Set<String>> result = new HashMap<String,Set<String>>();
		for(int i=0; i<rs.getNumberOfSolutions(); i++){
			Map<String, String> vars = rs.getSolution(i);
			String domain = vars.get("domain");
			String range = vars.get("range");
			Set<String> domains = result.get(range);
			if(domains==null){
				domains = new HashSet<String>(1);
				result.put(range, domains);
			}
			domains.add(domain);
		}
		return result;
	}

	@Override
	public boolean isAutoInsertable() {
		return false;
	}
	
}
