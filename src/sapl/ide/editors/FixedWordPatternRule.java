package sapl.ide.editors;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.WordPatternRule;


/**
 * WordPatternRule seems to have a bug of not checking for EOF, thus overriding all
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (09.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class FixedWordPatternRule extends WordPatternRule{
	
	public FixedWordPatternRule(IWordDetector detector, String startSequence, String endSequence, IToken token) {
		super(detector, startSequence, endSequence, token);
	}

	protected StringBuffer fBuffer2 = new StringBuffer();

	protected boolean endSequenceDetected(ICharacterScanner scanner) {

		fBuffer2.setLength(0);
		int c = scanner.read();
		while (c!=ICharacterScanner.EOF && fDetector.isWordPart((char) c)) {
			fBuffer2.append((char) c);
			c = scanner.read();
		}
		scanner.unread();

		if (fBuffer2.length() >= fEndSequence.length) {
			for (int i=fEndSequence.length - 1, j= fBuffer2.length() - 1; i >= 0; i--, j--) {
				if (fEndSequence[i] != fBuffer2.charAt(j)) {
					unreadBuffer(scanner);
					return false;
				}
			}
			return true;
		}
		unreadBuffer(scanner);
		return false;
	}

	protected void unreadBuffer(ICharacterScanner scanner) {
		fBuffer2.insert(0, fStartSequence);
		for (int i= fBuffer2.length() - 1; i > 0; i--)
			scanner.unread();
	}			

}
