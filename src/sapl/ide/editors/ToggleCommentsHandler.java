package sapl.ide.editors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (26.09.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class ToggleCommentsHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		ITextEditor editor = (ITextEditor)part;
		IDocumentProvider dp = editor.getDocumentProvider();
		IDocument doc = dp.getDocument(editor.getEditorInput());

		ITextSelection selection =  (ITextSelection)editor.getSelectionProvider().getSelection();

		try {
			
			int startLine = selection.getStartLine();
			int endLine = selection.getEndLine();
			int startLineOffset = 0;

			boolean toUnComment = true;
			for(int line=startLine; line<=endLine; line++){
				int offset;
				offset = doc.getLineOffset(line);
				if(line==startLine) startLineOffset = offset;
				int firstCharOffset = getOffsetOfNextNotWhitespaceChar(doc,offset);
				if(firstCharOffset==-1 || doc.getChar(firstCharOffset)!='/' && doc.getChar(firstCharOffset+1)!='/'){
					toUnComment = false;
					break;
				}
			}
			int endLineEnd = doc.getLineOffset(endLine)+doc.getLineLength(endLine);

			String text = doc.get(startLineOffset,endLineEnd-startLineOffset);

			if(!toUnComment){
				text = "//"+text.replace("\n", "\n//");
				text = text.substring(0,text.length()-2);
				doc.replace(startLineOffset, endLineEnd-startLineOffset, text);
			}
			else{
				text = text.replace("\n//", "\n");
				if(text.startsWith("//"))
					text = text.substring(2);
				int ind = -2;
				while((ind = text.indexOf("//",ind+2)) != -1){
					int ind2 = text.lastIndexOf("\n", ind);
					if(text.substring(ind2+1,ind).trim().isEmpty()){
						text = text.substring(0,ind)+text.substring(ind+2);
						ind=ind-2;
					}
				}
				
				doc.replace(startLineOffset, endLineEnd-startLineOffset, text);
			}

			if(selection.getLength()>0){
				selection = new TextSelection(startLineOffset, doc.getLineOffset(endLine)+doc.getLineLength(endLine)-startLineOffset);  
				editor.getSelectionProvider().setSelection(selection);
			}

		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getOffsetOfNextNotWhitespaceChar(IDocument doc, int offset) throws BadLocationException{
		int pos = offset;
		while (pos<doc.getLength() && Character.isWhitespace(doc.getChar(pos)) && !(doc.getChar(pos)=='\n')) pos++;
		if(Character.isWhitespace(doc.getChar(pos))) return -1;
		else return pos;
	}

}
