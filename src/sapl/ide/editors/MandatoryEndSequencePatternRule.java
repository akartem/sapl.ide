package sapl.ide.editors;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.PatternRule;

/**
 * PatternRule returns true on EOL (with fBreaksOnEOL==true).
 * This fix makes a rule that returns false then,
 * that is makes end sequence mandatory
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (01.12.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class MandatoryEndSequencePatternRule extends PatternRule{

	protected char[][] fLineDelimiters;
	
	protected char[][] fSortedLineDelimiters;
	
	char breaksOnChar;
	
	@SuppressWarnings("rawtypes")
	protected Comparator fLineDelimiterComparator= new DecreasingCharArrayLengthComparator();
	
	@SuppressWarnings("rawtypes")
	protected static class DecreasingCharArrayLengthComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			return ((char[]) o2).length - ((char[]) o1).length;
		}
	}	
	
	public MandatoryEndSequencePatternRule(String startSequence, String endSequence,
			IToken token, char escapeCharacter, boolean breaksOnEOL, char breaksOnChar) {
		super(startSequence, endSequence, token, escapeCharacter, breaksOnEOL);
		
		this.breaksOnChar = breaksOnChar; 
	}
	
	@SuppressWarnings("unchecked")
	protected boolean endSequenceDetected(ICharacterScanner scanner) {

		char[][] originalDelimiters= scanner.getLegalLineDelimiters();
		int count= originalDelimiters.length;
		if (fLineDelimiters == null || fLineDelimiters.length != count) {
			fSortedLineDelimiters= new char[count][];
		} else {
			while (count > 0 && Arrays.equals(fLineDelimiters[count - 1], originalDelimiters[count - 1]))
				count--;
		}
		if (count != 0) {
			fLineDelimiters= originalDelimiters;
			System.arraycopy(fLineDelimiters, 0, fSortedLineDelimiters, 0, fLineDelimiters.length);
			Arrays.sort(fSortedLineDelimiters, fLineDelimiterComparator);
		}

		int readCount= 1;
		int c;
		while ((c= scanner.read()) != ICharacterScanner.EOF) {
			//THIS LINE ADDED
			if(breaksOnChar!=0 && c==breaksOnChar)
				break;
			
			if (c == fEscapeCharacter) {
				// Skip escaped character(s)
				if (fEscapeContinuesLine) {
					c= scanner.read();
					for (int i= 0; i < fSortedLineDelimiters.length; i++) {
						if (c == fSortedLineDelimiters[i][0] && sequenceDetected(scanner, fSortedLineDelimiters[i], true))
							break;
					}
				} else
					scanner.read();

			} else if (fEndSequence.length > 0 && c == fEndSequence[0]) {
				// Check if the specified end sequence has been found.
				if (sequenceDetected(scanner, fEndSequence, true))
					return true;
			} else if (fBreaksOnEOL) {
				// Check for end of line since it can be used to terminate the pattern.
				for (int i= 0; i < fSortedLineDelimiters.length; i++) {
					if (c == fSortedLineDelimiters[i][0] && sequenceDetected(scanner, fSortedLineDelimiters[i], true))
						//THIS LINE IS CHANGED
						return false;
				}
			}
			readCount++;
		}

		if (fBreaksOnEOF)
			//THIS LINE IS CHANGED
			return false;

		for (; readCount > 0; readCount--)
			scanner.unread();

		return false;
	}	

}
