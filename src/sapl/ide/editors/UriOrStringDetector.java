package sapl.ide.editors;

import org.eclipse.jface.text.rules.IWordDetector;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (09.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class UriOrStringDetector implements IWordDetector{

	@Override
	public boolean isWordStart(char c) {
		return isWordPart(c);
	}

	@Override
	public boolean isWordPart(char c) {
		return c!=' ' && c!='\t' && c!='\n' && c!='\r' 
				&& c!='{' && c!='}' && c!='[' && c!=']';
	}
	
}
