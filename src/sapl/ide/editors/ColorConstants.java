package sapl.ide.editors;

import org.eclipse.swt.graphics.RGB;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (22.09.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public interface ColorConstants {
	RGB DEFAULT = new RGB(0, 0, 0);
	RGB COMMENT = new RGB(63, 127, 95);
	RGB STRING = new RGB(42, 0, 255);
	RGB VARIABLE = new RGB(42, 0, 255);
	RGB URI = new RGB(128, 128, 128);
	RGB PREFIX = new RGB(0, 128, 0);
	RGB KEYWORD = new RGB(127, 0, 85);
}
