package sapl.ide.editors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension4;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;

import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.ide.Activator;
import sapl.ide.util.RdfClassGenerator;
import sapl.ide.util.TableDialog;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (20.10.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class RdfsClassGenerationProposal extends ExtensibleCompletionProposal implements ICompletionProposalExtension4{

	String lastIndent;

	public RdfsClassGenerationProposal(String lastIndent,
			int replacementOffset, int replacementLength, int cursorPosition,
			Image image, String displayString,
			IContextInformation contextInformation,
			String additionalProposalInfo) {

		super("", replacementOffset, replacementLength, cursorPosition,
				image, displayString, contextInformation, additionalProposalInfo);
		
		this.lastIndent = lastIndent;
		
	}

	@Override
	public boolean isAutoInsertable() {
		return false;
	}

	@Override
	public void apply(IDocument document) {
		fReplacementString = produceReplacementString(document);
		if(fReplacementString!=null){
			try {
				document.replace(fReplacementOffset, fReplacementLength, fReplacementString);
			} catch (BadLocationException x) {
			}
		}
	}

	private String produceReplacementString(IDocument document) {
		try {
			String content = document.get();
			SaplCode saplCode;
			try {
				saplCode = SaplN3Parser.compile(content,"document");
			}
			catch (Throwable e){
				String message = "The file has syntax errors: cannot proceed!";
				Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}
	
			SaplModel model = new SaplModel(false, false);
			SaplN3Parser.load(saplCode.getCode(), model, SaplConstants.GENERAL_CONTEXT, null, "document", false);
	
			String query = "{?cl a <"+SaplConstants.RDFS_NS+"Class>} <"+SaplConstants.SAPL_NS+"or> {?cl a <"+SaplConstants.OWL_NS+"Class>}";
			SaplQueryResultSet rs = new SaplQueryResultSet();
			boolean r = SaplQueryEngine.evaluateQueryN3(query, SaplConstants.GENERAL_CONTEXT, model, false, rs);
			if(!r || rs.getNumberOfSolutions()==0){
				String message = "No rdfs:Class or owl:Class definitions can be found!";
				MessageDialog.openInformation(new Shell(),"S-APL IDE",message);		
				return null;
			}
			
			Map<String,String> classes = new HashMap<String,String>();
			List<String> classNames = new ArrayList<String>();
			
			List<List<String>> table = new ArrayList<List<String>>();
			List<String> header = new ArrayList<String>();
	    	header.add("Class");
	    	header.add("URI");
	    	table.add(header);

	    	for(int i=0; i<rs.getNumberOfSolutions(); i++){
	    		Map<String, String> sol = rs.getSolution(i);
	    		String classID = sol.get("cl");
	    		String prefixedID = RdfClassGenerator.prefix(classID);
	    		classes.put(prefixedID, classID);
	    		classNames.add(prefixedID);
	    	}
	    	
	    	Collections.sort(classNames);
	    	
	    	for(String classID : classNames){
		    	List<String> row = new ArrayList<String>(); 
		    	row.add(classID);
		    	row.add(classes.get(classID));
		    	table.add(row);
	    	}
	
			TableDialog dialog = new TableDialog(new Shell());
	    	dialog.setText("Select a class");
	    	dialog.setTableData(table);
	    	int selected = dialog.open();
	    	if(selected==-1) return null;
	    	
	    	String selectedClass = table.get(selected+1).get(0);
	    	
	    	String definition = RdfClassGenerator.generate(classes.get(selectedClass), model, null);

			//definition = definition.replace("\n", "\n"+this.lastIndent);
	    	return definition;
		} catch (Exception e) {
			String message = "An exception occured! See the Error Log view.";
			Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, Status.ERROR, message, e));
			MessageDialog.openInformation(new Shell(),"S-APL IDE",message);			
		}
		return null;
		
	}

}
