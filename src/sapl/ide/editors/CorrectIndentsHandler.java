package sapl.ide.editors;

import java.util.Stack;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (25.05.2015)
  * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class CorrectIndentsHandler extends AbstractHandler {

	String tabs = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

	private Indent indent;

	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		ITextEditor editor = (ITextEditor)part;
		IDocumentProvider dp = editor.getDocumentProvider();
		IDocument doc = dp.getDocument(editor.getEditorInput());

		ITextSelection selection = (ITextSelection)editor.getSelectionProvider().getSelection();

	    ITextOperationTarget target = (ITextOperationTarget)editor.getAdapter(ITextOperationTarget.class);
        ITextViewer textViewer = (ITextViewer)target;
		int topVisible = textViewer.getTopIndex();
		int bottomVisible = textViewer.getBottomIndex();
	        
		try {

			int startLine = selection.getStartLine();
			int endLine = selection.getEndLine();
			int startLineOffset = 0;

			indent = new Indent("","");
			String text = "";

			//find base indent (of last line before selected ones)
			for(int line = startLine-1; line>=0; line--){
				int offset = doc.getLineOffset(line);
				int firstCharOffset = getOffsetOfNextNotWhitespaceChar(doc, offset);
				if(firstCharOffset!=-1){
					String str = doc.get(offset, doc.getLineLength(line)).trim();
					if(!str.startsWith("//") && !str.startsWith("/*") && !str.startsWith("#")){
						indent.baseIndent = doc.get(offset, firstCharOffset-offset);
						adjustIndentForNextLine(str);
						break;
					}
				}
			}

			//fix indents of selected lines
			boolean insideMultilineComment = false;
			for(int line = startLine; line <= endLine; line++){
				int offset = doc.getLineOffset(line);
				if(line==startLine) startLineOffset = offset;
				int firstCharOffset = getOffsetOfNextNotWhitespaceChar(doc, offset);
				String str = doc.get(offset, doc.getLineLength(line));
				if(firstCharOffset!=-1){
					String trimmed = str.trim();
					if(!insideMultilineComment && !trimmed.startsWith("//") 
							&& !trimmed.startsWith("/*") && !trimmed.startsWith("#")){				
						adjustIndentForThisLine(str);
						str = indent.baseIndent+indent.extraIndent+trimmed+"\r\n";
						adjustIndentForNextLine(str);
					}
					else{
						if(Character.isWhitespace(str.charAt(0)))
							str = indent.baseIndent+indent.extraIndent+trimmed+"\r\n";
					}
					
					int indS = str.lastIndexOf("/*");
					int indE = str.lastIndexOf("*/");
					if(indE!=-1) insideMultilineComment = false;
					if(indS!=-1 && indS>indE) insideMultilineComment = true;
				}
				text += str;
			}

			int endLineEnd = doc.getLineOffset(endLine)+doc.getLineLength(endLine);			

			doc.replace(startLineOffset, endLineEnd-startLineOffset, text);

			if(selection.getLength()>0){
				selection = new TextSelection(startLineOffset, doc.getLineOffset(endLine)+doc.getLineLength(endLine)-startLineOffset);  
				editor.getSelectionProvider().setSelection(selection);
			}
			
			if(startLine < topVisible){
				int visibleOffset = doc.getLineOffset(topVisible);
				textViewer.revealRange(visibleOffset, doc.getLineOffset(bottomVisible)+doc.getLineLength(bottomVisible)-visibleOffset);
			}

		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getOffsetOfNextNotWhitespaceChar(IDocument doc, int offset) throws BadLocationException{
		int pos = offset;
		while (pos<doc.getLength() && Character.isWhitespace(doc.getChar(pos)) && !(doc.getChar(pos)=='\n')) pos++;
		if(pos==doc.getLength() || Character.isWhitespace(doc.getChar(pos))) return -1;
		else return pos;
	}

	private void adjustIndentForThisLine(String str){
		String newIndent = indent.baseIndent;
		int score = 0;
		int ind = -1;
		while(true){
			int indOpen = str.indexOf('{',ind+1);
			if(indOpen==-1) indOpen = Integer.MAX_VALUE; 
			int indOpen2 = str.indexOf('[',ind+1);
			if(indOpen2==-1) indOpen2 = Integer.MAX_VALUE; 
			indOpen=Math.min(indOpen, indOpen2);

			int indClose = str.indexOf('}',ind+1);
			if(indClose==-1) indClose=Integer.MAX_VALUE;
			int indClose2 = str.indexOf(']',ind+1);
			if(indClose2==-1) indClose2=Integer.MAX_VALUE;
			indClose=Math.min(indClose, indClose2);

			if(indOpen==Integer.MAX_VALUE && indClose==Integer.MAX_VALUE) break;

			if(indOpen < indClose){
				break;
			}
			else{
				score--;
				ind = indClose;
			}
		}
		if(score<0){
			if(-1*score > newIndent.length()) score = -1*newIndent.length();
			newIndent = newIndent.substring(0, newIndent.length()+score);
			indent.baseIndent = newIndent;
			indent.extraIndent = "";
			indent.depth += score;
			indent.lastChange = -1;

			if(indent.depth<=-1 && !indent.outerIndents.isEmpty()){
				indent = indent.outerIndents.pop();
			}
		}
		else indent.lastChange = 0;
	}

	private void adjustIndentForNextLine(String str){
		String newIndent = indent.baseIndent;
		int score = 0;
		int ind = -1;
		boolean first = true;
		while(true){
			int indOpen = str.indexOf('{',ind+1);
			if(indOpen==-1) indOpen = Integer.MAX_VALUE; 
			int indOpen2 = str.indexOf('[',ind+1);
			if(indOpen2==-1) indOpen2 = Integer.MAX_VALUE; 
			indOpen=Math.min(indOpen, indOpen2);

			int indClose = str.indexOf('}',ind+1);
			if(indClose==-1) indClose=Integer.MAX_VALUE;
			int indClose2 = str.indexOf(']',ind+1);
			if(indClose2==-1) indClose2=Integer.MAX_VALUE;
			indClose=Math.min(indClose, indClose2);

			if(indOpen==Integer.MAX_VALUE && indClose==Integer.MAX_VALUE) break;

			if(indOpen < indClose){
				score++;
				ind = indOpen;
			}
			else{
				//if(score>0) score--;
				if(!first) score--;
				ind = indClose;
			}
			first=false;
		}
		
		if(score>0){
			newIndent += tabs.substring(0,score);

			if(!indent.extraIndent.isEmpty()){
				Indent temp = indent;
				indent = new Indent(newIndent+indent.extraIndent, "");
				indent.outerIndents.push(temp);
			}
			else{
				indent.baseIndent = newIndent;
				indent.depth += score;
			}
		}
		else if(score<0){
			if(-1*score > newIndent.length()) score = -1*newIndent.length();
			newIndent = newIndent.substring(0, newIndent.length()+score);
			indent.baseIndent = newIndent;
			indent.depth += score;
		}
			
		int com_ind = str.indexOf(" //");
		if(com_ind!=-1) str = str.substring(0,com_ind);
		String trimmed = str.trim();
		//if(score<=0 && !trimmed.endsWith("{") && !trimmed.endsWith("[") && !trimmed.endsWith(".") && (!trimmed.endsWith("}") || indent.lastChange!=-1)){ 
		if(!trimmed.endsWith("{") && !trimmed.endsWith(".") && (!trimmed.endsWith("}") || indent.lastChange!=-1)){ 
			indent.extraIndent = "\t";
		}
		else{
			indent.extraIndent = "";
		}		

	}

	private class Indent{
		public String baseIndent;
		public String extraIndent;

		int depth;

		public int lastChange;

		public Stack<Indent> outerIndents = new Stack<Indent>();

		Indent(String b, String e){
			baseIndent = b;
			extraIndent = e;
			lastChange = 0;
			depth = 0;
		}
	}

}
