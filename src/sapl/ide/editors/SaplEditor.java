package sapl.ide.editors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.source.DefaultCharacterPairMatcher;
import org.eclipse.jface.text.source.ICharacterPairMatcher;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;
import org.osgi.framework.Bundle;

import sapl.core.ExpressionEvaluator;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.ide.Activator;
import sapl.ide.util.Template;
import sapl.shared.eii.EiiConstants;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (20.10.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class SaplEditor extends TextEditor {

	private ColorManager colorManager;

	public SaplEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new SaplEditorConfiguration(colorManager));
		setDocumentProvider(new DocumentProvider());
	}

	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}

	public final static String EDITOR_MATCHING_BRACKETS = "matchingBrackets";
	public final static String EDITOR_MATCHING_BRACKETS_COLOR = "matchingBracketsColor";	
	/**Enable highlighting matching brackets*/
	@Override
	protected void configureSourceViewerDecorationSupport (SourceViewerDecorationSupport support) {
		super.configureSourceViewerDecorationSupport(support);		

		char[] matchChars = {'{', '}', '(', ')', '[', ']'}; //which brackets to match		
		ICharacterPairMatcher matcher = new DefaultCharacterPairMatcher(matchChars ,
				IDocumentExtension3.DEFAULT_PARTITIONING);
		support.setCharacterPairMatcher(matcher);
		support.setMatchingCharacterPainterPreferenceKeys(EDITOR_MATCHING_BRACKETS,EDITOR_MATCHING_BRACKETS_COLOR);
		IPreferenceStore store = getPreferenceStore();
		store.setDefault(EDITOR_MATCHING_BRACKETS, true);
		store.setDefault(EDITOR_MATCHING_BRACKETS_COLOR, "128,128,128");
	}	

	static final List<String> keywords = SaplEditor.createKeywords();
	/**Access S-APL keywords (s:) to use for highlighting and content assist*/
	static List<String> createKeywords() {
		ArrayList<String> words = new ArrayList<String>();
		for(String concept: SaplConstants.concepts){
			words.add(concept.substring(SaplConstants.SAPL_NS.length()));
		}
		Collections.sort(words);
		return Collections.unmodifiableList(words);
	}

	static final List<String> shorthands = SaplEditor.createShorthands();
	/**Access S-APL shorthands to use for highlighting and content assist*/
	static List<String> createShorthands() {
		ArrayList<String> words = new ArrayList<String>();
		for(String word : SaplN3Parser.replace.keySet())
			words.add(word);
		words.add(SaplN3Parser.PREFIX_KEYWORD);
		Collections.sort(words);
		return Collections.unmodifiableList(words);
	}

	static final List<String> ontoKeywords = SaplEditor.createOntoKeywords();
	/**Access ontonut keywords (o:) to use for content assist*/
	static List<String> createOntoKeywords() {
		ArrayList<String> words = new ArrayList<String>();
		for(String concept: EiiConstants.concepts){
			words.add(concept.substring(SaplConstants.ONTONUT_NS.length()));
		}
		Collections.sort(words);
		return Collections.unmodifiableList(words);
	}
	
	static final Map<String,String> functions = SaplEditor.createFunctions();
	/**Access S-APL shorthands to use for highlighting and content assist*/
	static Map<String,String> createFunctions() {
		LinkedHashMap<String,String> funcs = new LinkedHashMap<String,String>();
		ArrayList<String> words = new ArrayList<String>();
		for(String word : ExpressionEvaluator.functions.keySet())
			words.add(word);
		Collections.sort(words);
		for(String word : words)
			funcs.put(word,ExpressionEvaluator.functions.get(word));
		return Collections.unmodifiableMap(funcs);
	}
	
	static final List<String> rabs = SaplEditor.createRabs();
	/**Find all known RABs*/
	static List<String> createRabs() {
		ArrayList<String> words = new ArrayList<String>();
		Bundle b = Platform.getBundle("SAPL2");
		Enumeration<URL> entries = b.findEntries("", "SAPL2.jar", true);
		if(entries!=null){
			URL url = entries.nextElement(); 
			ZipInputStream zip;
			try {
				zip = new ZipInputStream(url.openStream());
				while(true) {
					ZipEntry e = zip.getNextEntry();
					if (e == null) break;
					String name = e.getName();
					if(name.startsWith("sapl/shared") && name.endsWith("Behavior.class")){
						words.add(name.substring(0,name.length()-6).replace("/","."));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		entries = b.findEntries("", "*.class", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String name = url.toString();
				if(name.contains("sapl/shared/") && name.endsWith("Behavior.class")){
					name = name.substring(name.indexOf("sapl/shared/"), name.length()-6).replace("/",".");
					words.add(name);
				}
			}
		}

		//		CodeSource src = SaplN3Parser.class.getProtectionDomain().getCodeSource();
		//		if(src!=null){
		//			URL jar = src.getLocation();
		//			System.out.println(jar);
		//			ZipInputStream zip;
		//			try {
		//				zip = new ZipInputStream(jar.openStream());
		//				while(true) {
		//					ZipEntry e = zip.getNextEntry();
		//					if (e == null) break;
		//					String name = e.getName();
		//					if(name.startsWith("sapl/shared") && name.endsWith("Behavior.class")){
		//						words.add(name.substring(0,name.length()-6).replace("/","."));
		//					}
		//				}
		//			} catch (IOException e) {
		//				e.printStackTrace();
		//			}
		//		}
		
		Collections.sort(words);
		return Collections.unmodifiableList(words);
	}
	
	static public final List<Template> templates = SaplEditor.createTemplates();
	/**Find all templates*/
	static List<Template> createTemplates() {
		ArrayList<Template> templates = new ArrayList<Template>();
		Bundle b = Platform.getBundle(Activator.PLUGIN_ID);
		Enumeration<URL> entries = b.findEntries("", "*.sapl", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String filename = url.toString();
				filename = filename.substring(filename.lastIndexOf('/')+1);
				String content = "";
				String name = filename;
				try {
				    InputStream inputStream = url.openConnection().getInputStream();
				    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
				    String inputLine;
				    
				    boolean first = true;
				    while ((inputLine = in.readLine()) != null) {
				    	if(first && inputLine.startsWith("//"))
			    			name = inputLine.substring(2).trim();
				    	else content += inputLine+'\n';
				    	first = false;
				    }
				    in.close();
				    
			    	templates.add(new Template(name, content));
				    
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		Collections.sort(templates);
		return Collections.unmodifiableList(templates);
	}
	
	static final Map<String,String> help = SaplEditor.createHelp();
	/**Access S-APL ontology to obtain comments on concepts and functions*/
	static Map<String,String> createHelp() {
		Map<String,String> words = new HashMap<String,String>();
		Bundle b = Platform.getBundle("SAPL2");
		Enumeration<URL> entries = b.findEntries("ontology", "*.sapl", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String filename = url.toString();
				filename = filename.substring(filename.indexOf("ontology"));
				try {
				    InputStream inputStream = url.openConnection().getInputStream();
				    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
				    String content="";
				    String inputLine;
				    while ((inputLine = in.readLine()) != null) {
				    	content += inputLine+"\n";
				    }
				    in.close();
				    
				    SaplCode code = SaplN3Parser.compile(content, filename);
					SaplModel model = new SaplModel(false, false);
					SaplN3Parser.load(code.getCode(), model, SaplConstants.GENERAL_CONTEXT, null, filename, false);
					SaplQueryResultSet rs = new SaplQueryResultSet();
					SaplQueryEngine.evaluateQueryN3("?concept <http://www.w3.org/2000/01/rdf-schema#comment> ?comment", SaplConstants.GENERAL_CONTEXT, model, false, rs);
					for(int i=0; i<rs.getNumberOfSolutions();i++){
						Map<String, String> sol = rs.getSolution(i);
						String concept = sol.get("concept");
						String comment = sol.get("comment").trim();
						words.put(concept, comment);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}					
		return Collections.unmodifiableMap(words);
	}
	
	
}
