package sapl.ide.editors;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (22.09.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class WhitespaceDetector implements IWhitespaceDetector {
	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}
}
