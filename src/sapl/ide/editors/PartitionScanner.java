package sapl.ide.editors;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.0 (09.10.2014)
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class PartitionScanner extends RuleBasedPartitionScanner {
	public final static String SAPL_MULTILINE_COMMENT = "__sapl_multiline_comment";
	public final static String SAPL_MULTILINE_STRING = "__sapl_multiline_string";
	public final static String SAPL_SINGLELINE_COMMENT = "__sapl_singleline_string";
	public final static String SAPL_URI = "__sapl_uri";

	public PartitionScanner() {

		IToken singlelineComment = new Token(SAPL_SINGLELINE_COMMENT);
		IToken multilineComment = new Token(SAPL_MULTILINE_COMMENT);
		IToken multilineString = new Token(SAPL_MULTILINE_STRING);
		IToken uri = new Token(SAPL_URI);

		IPredicateRule[] rules = new IPredicateRule[6];

		rules[0] = new MultiLineRule("/*", "*/", multilineComment);
		rules[1] = new MandatoryEndSequencePatternRule("<", ">", uri, '\\',true,' ');
		rules[2] = new EndOfLineRule("//", singlelineComment);
		rules[3] = new EndOfLineRule("#", singlelineComment);
		rules[4] = new MultiLineRule("\"", "\"", multilineString, '\\');
		rules[5] = new MultiLineRule("'", "'", multilineString, '\\');

		setPredicateRules(rules);
	}
}
