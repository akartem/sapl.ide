package sapl.ide.wizards;

import sapl.ide.editors.SaplEditor;
import sapl.ide.util.Template;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (29.04.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class HelloWorldSaplFileWizard extends SaplFileWizard {
	
	@Override
	protected String addContent(){
		String result = "";
		for(Template t : SaplEditor.templates){
			if(t.name.equals("hello.sapl")) 
				result = t.template;
		}		
		
		return result;
	} 
}
