package sapl.ide.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

import sapl.core.SaplConstants;
import sapl.core.sapln3.SaplN3Parser;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.6 (26.05.2018)
 * 
 * Copyright (C) 2014-2018, VTT
 * 
 * */

public class SaplFileWizard extends BasicNewFileResourceWizard{
	@Override
	public boolean performFinish() {

		WizardNewFileCreationPage page = (WizardNewFileCreationPage)this.getStartingPage();
		String name = page.getFileName();

		if(!name.endsWith(".sapl")) name=name+".sapl";
		page.setFileName(name);

		IFile file = page.createNewFile();
		if (file == null) return false;

		selectAndReveal(file);

		// Open editor on new file.
		IWorkbenchWindow dw = getWorkbench().getActiveWorkbenchWindow();
		try {
			if (dw != null) {
				IWorkbenchPage active = dw.getActivePage();
				if (page != null) {
					IEditorPart part = IDE.openEditor(active, file, true);
					
					//add some text
					if (part instanceof AbstractTextEditor){
						ITextEditor editor = (ITextEditor)part;
						IDocumentProvider dp = editor.getDocumentProvider();
						IDocument doc = dp.getDocument(editor.getEditorInput());
						
						String text = SaplN3Parser.PREFIX_KEYWORD+" "+"s: <"+SaplConstants.SAPL_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"j: <"+SaplConstants.RAB_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"p: <"+SaplConstants.PARAM_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"o: <"+SaplConstants.ONTONUT_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"d: <"+SaplConstants.DATA_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"m: <"+SaplConstants.META_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"t: <"+SaplConstants.TECH_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"rdf: <"+SaplConstants.RDF_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"rdfs: <"+SaplConstants.RDFS_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"xsd: <"+SaplConstants.XSD_NS+">.\n";
						text += SaplN3Parser.PREFIX_KEYWORD+" "+"owl: <"+SaplConstants.OWL_NS+">.\n";
						text += "\n";
						
						text += addContent();
						
						try {
							doc.replace(0, 0, text);
						} catch (BadLocationException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (PartInitException e) {
			e.printStackTrace();
		}

		return true;
	}
	
	protected String addContent(){
		String text = "";
		text += "<./rules/Ontonutty.sapl> s:is s:true .\n";
		text += "<./rules/Classifier.sapl> s:is s:true .\n";
		text += "\n";
		return text;
	} 
}
