package sapl.ide.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (29.05.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class RABWizard extends BasicNewFileResourceWizard{
	@Override
	public boolean performFinish() {

		WizardNewFileCreationPage page = (WizardNewFileCreationPage)this.getStartingPage();
		String name = page.getFileName();

		if(!name.endsWith(".java")) name=name+".java";
		name = name.substring(0,1).toUpperCase()+name.substring(1);
		page.setFileName(name);

		IFile file = page.createNewFile();
		if (file == null) return false;

		selectAndReveal(file);

		// Open editor on new file.
		IWorkbenchWindow dw = getWorkbench().getActiveWorkbenchWindow();
		try {
			if (dw != null) {
				IWorkbenchPage active = dw.getActivePage();
				if (page != null) {
					IEditorPart part = IDE.openEditor(active, file, true);
					
					//add some text
					if (part instanceof AbstractTextEditor){
						ITextEditor editor = (ITextEditor)part;
						IDocumentProvider dp = editor.getDocumentProvider();
						IDocument doc = dp.getDocument(editor.getEditorInput());
						
						String text = "import sapl.core.ReusableAtomicBehavior;\n";
						text += "import sapl.core.SaplConstants;\n";
						text += "import sapl.util.URITools;\n";
						text += "import sapl.core.rab.BehaviorStartParameters;\n";
						text += "import sapl.core.rab.Parameter;\n";
						text += "import sapl.core.rab.IllegalParameterConfigurationException;\n";
						text += "import sapl.core.rab.Resource;\n";
						text += "\n";
						text += "public class "+name.replace(".java","")+" extends ReusableAtomicBehavior {\n";
						text += "\n";
						text += "\t@Parameter (name=\"param\") private String parameter;\n";
						text += "\n";
						text += "\tprotected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException{\n";
						text += "\t\t//TODO Access start-up parameters here\n"; 
						text += "\t\tthis.parameter = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS),\"param\"), \"\");\n";
						text += "\t}\n";
						text += "\n";
						text += "\tprotected void doAction() throws Exception {\n";
						text += "\t\t//TODO Behavior body goes here\n"; 
						text += "\t}\n";
						text += "}";
						text += "\n";
						
						try {
							doc.replace(0, 0, text);
						} catch (BadLocationException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (PartInitException e) {
			e.printStackTrace();
		}


		return true;
	}
}
