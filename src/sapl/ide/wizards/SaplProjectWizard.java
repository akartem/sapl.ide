package sapl.ide.wizards;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.osgi.framework.Bundle;

/**
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.1 (25.05.2015)
 * 
 * Copyright (C) 2014-2015, VTT
 * 
 * */

public class SaplProjectWizard extends BasicNewProjectResourceWizard{

	private static final String NATURE_ID = "sapl.checker.saplNature";
	
	@Override
	public boolean performFinish() {
		if (!super.performFinish())
			return false;
		
		IProject newProject = this.getNewProject();
		
		try {

			IProjectDescription description = newProject.getDescription();
			String[] newNatures = new String[2];
			newNatures[0] = JavaCore.NATURE_ID;
			newNatures[1] = NATURE_ID;
			description.setNatureIds(newNatures);
			newProject.setDescription(description, null);

			IJavaProject javaProject = JavaCore.create(newProject);

			IFolder sourceFolder = newProject.getFolder("src");
			sourceFolder.create(false, true, null);

			IPath targetPath = javaProject.getPath().append("bin");
			javaProject.setOutputLocation(targetPath, null);
			
			Set<IClasspathEntry> cp_entries = new HashSet<IClasspathEntry>();
			IClasspathEntry[] existingEntries = javaProject.getRawClasspath();
			for(IClasspathEntry entry : existingEntries){
				if(entry.getEntryKind()!=IClasspathEntry.CPE_SOURCE){
					cp_entries.add(entry);
				}
			}
			cp_entries.add(JavaRuntime.getDefaultJREContainerEntry());
			for(String lib : classpathEntries){
				cp_entries.add(JavaCore.newLibraryEntry(new Path(lib), null, null));
			}
			
			IClasspathEntry srcEntry = JavaCore.newSourceEntry(new Path("/"+newProject.getName()+"/src/"));
			cp_entries.add(srcEntry);
			
			javaProject.setRawClasspath(cp_entries.toArray(new IClasspathEntry[cp_entries.size()]), null);				
			
			IFolder saplFolder = newProject.getFolder("sapl");
			if(!saplFolder.exists()) saplFolder.create(true,true,null);

			IFolder folder = newProject.getFolder("rules");
			if(!folder.exists()) folder.create(true,true,null);

			Bundle b = Platform.getBundle("SAPL2");
			Enumeration<URL> entries = b.findEntries("rules", "*.sapl", true);
			if(entries!=null){
				while (entries.hasMoreElements()){
					URL url = entries.nextElement(); 
					String filename = url.toString();
					filename = filename.substring(filename.indexOf("rules"));
					try {
					    InputStream inputStream = url.openConnection().getInputStream();
					    BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
					    String content="";
					    String inputLine;
					    while ((inputLine = in.readLine()) != null) {
					    	content += inputLine+"\n";
					    }
					    in.close();
					    
						IFile shFile = newProject.getFile(filename);
						if(shFile.exists()) shFile.delete(true, null); 
						shFile.create(new ByteArrayInputStream(content.getBytes()),true,null);

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}					
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	static final List<String> classpathEntries = SaplProjectWizard.createClasspathEntries();
	static List<String> createClasspathEntries() {

		Bundle b = Platform.getBundle("SAPL2");
		String id = String.valueOf(b.getBundleId());
		
		String eclipseHome = System.getProperty("eclipse.home.location");
		eclipseHome = eclipseHome.substring(eclipseHome.indexOf("file:")+6);
		
		String bundleLocation = b.getLocation();
		
		int ind = bundleLocation.indexOf(eclipseHome);
		if(ind!=-1) bundleLocation = bundleLocation.substring(ind);
		else{
			int ind2 = bundleLocation.indexOf("dropins");
			if(ind2==-1) bundleLocation.indexOf("plugins");
			if(ind2!=-1)
				bundleLocation = eclipseHome + bundleLocation.substring(ind2);
			else bundleLocation = bundleLocation.substring(bundleLocation.indexOf("file:")+6);
		}
		
		ArrayList<String> words = new ArrayList<String>();
		Enumeration<URL> entries = b.findEntries("", "*.jar", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String name = url.toString();
				int ind3 = name.indexOf(id);
				name = bundleLocation+name.substring(name.indexOf('/', ind3)+1);
				words.add(name);
			}
		}
		entries = b.findEntries("", "bin", true);
		if(entries!=null){
			while (entries.hasMoreElements()){
				URL url = entries.nextElement(); 
				String name = url.toString();
				int ind3 = name.indexOf(id);
				name = bundleLocation+name.substring(name.indexOf('/', ind3)+1);
				words.add(name);
			}
		}
		
		return Collections.unmodifiableList(words);
	}	
	
}
