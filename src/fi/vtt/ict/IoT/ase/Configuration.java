package fi.vtt.ict.IoT.ase;

import fi.vtt.ict.IoT.ase.methods.SynthesizedAlignmentMethod;
import fi.vtt.ict.IoT.ase.methods.Level1NamesLabelsCommentsSynthesizedAlignmentMethod;
import fi.vtt.ict.IoT.ase.methods.Level2NamesLabelsSynthesizedAlignmentMethod;
import fi.vtt.ict.IoT.ase.methods.Level3NamesLabelsSynthesizedAlignmentMethod;
import fr.inrialpes.exmo.align.impl.BasicAlignment;

import java.net.URI;

/**
 * Alignment methods of ASE (Alignment of Smart Entities)
 * 
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Configuration {

	//ASE configuration used for the IoT project and the 'smart proxy' software
	public BasicAlignment setConfAseSynthesizedAlignmentMethod(URI onto1, URI onto2) throws Exception {

		long s = System.currentTimeMillis();

		BasicAlignment synthAlignment = new BasicAlignment();
		long now;

		//First level - synthesized method of alignment (string similarity methods using names, labels, comments)

		SynthesizedAlignmentMethod level1NamesLabelscommentsSAM = new Level1NamesLabelsCommentsSynthesizedAlignmentMethod();
		synthAlignment = level1NamesLabelscommentsSAM.computeMethod(onto1, onto2);

		now = System.currentTimeMillis();
		System.out.println("Lexical: "+synthAlignment.getArrayElements().size()+" ["+(now-s)+"]");
		s = now;

		//Second level - synthesized method of alignment (semantic similarity method using WordNet to identify synonyms - works with names and labels)
		SynthesizedAlignmentMethod level2NamesLabelsSAM = new Level2NamesLabelsSynthesizedAlignmentMethod();
		BasicAlignment level2NamesLabelsAlignment = level2NamesLabelsSAM.computeMethod(onto1, onto2);

		now = System.currentTimeMillis();
		System.out.println("WordNet synonyms: "+level2NamesLabelsAlignment.getArrayElements().size()+" ["+(now-s)+"]");
		s = now;

		synthAlignment.ingest(level2NamesLabelsAlignment);


		//Third level - synthesized method of alignment (semantic similarity method using WordNet to identify subsumptions - works with names and labels)
		SynthesizedAlignmentMethod level3NamesLabelsSAM = new Level3NamesLabelsSynthesizedAlignmentMethod();
		BasicAlignment level3NamesLabelsAlignment = level3NamesLabelsSAM.computeMethod(onto1, onto2);

		now = System.currentTimeMillis();
		System.out.println("WordNet subsumption: "+level3NamesLabelsAlignment.getArrayElements().size()+" ["+(now-s)+"]");
		s = now;

		synthAlignment.ingest(level3NamesLabelsAlignment);

		return synthAlignment;

	}

	//conf1 synthesized method (four levels of alignment) - differs from conf2 the instance-based level alignment ie. the level five
	//    public BasicAlignment setConf1SynthesizedAlignmentMethod(URI onto1, URI onto2) throws AlignmentException, IOException {
	//
	//        BasicAlignment synthAlignment = new BasicAlignment();
	//
	//        //First level alignment (Simple lexical methods)
	//        BasicAlignment level1Alignment = new BasicAlignment();
	//        Level1SynthesizedAlignmentMethod level1SAM = new Level1SynthesizedAlignmentMethod();
	//        level1Alignment = level1SAM.computeMethod(onto1, onto2);
	// 
	//        //Second level alignment (Lexical methods using WordNet)
	//        BasicAlignment level2Alignment = new BasicAlignment();
	//        Level2SynthesizedAlignmentMethod level2SAM = new Level2SynthesizedAlignmentMethod();
	//        level2Alignment = level2SAM.computeMethod(onto1, onto2);
	//
	//        //synthesize the first two levels of alignment methods i.e.StringSimilarity and WNsimilarity methods // Clone first level alignment results 
	//        //also trim to 0.8
	//        BasicAlignment TwoLevelAlignment = (BasicAlignment) (level1Alignment.clone());
	//        TwoLevelAlignment.ingest(level2Alignment);
	//        
	//        //Third level alignment (Semantic distances via Vector Space Models)
	//        BasicAlignment level3Alignment = new BasicAlignment();
	//        Level3SynthesizedAlignmentMethod level3SAM = new Level3SynthesizedAlignmentMethod();
	//        level3Alignment = level3SAM.computeMethod(onto1, onto2);
	//
	//        //Synthesize level three alignment result to previous synthesized alignment level i.e. TwoLevelAlignment
	//        //also trim to 0.8
	//        BasicAlignment ThreeLevelAlignment = (BasicAlignment) (level3Alignment.clone());
	//        ThreeLevelAlignment.ingest(TwoLevelAlignment);
	//                     
	//        //Fourth level alignment (structure-based alignment)
	//        BasicAlignment level4Alignment = new BasicAlignment();
	//        Level4SynthesizedAlignmentMethod level4SAM = new Level4SynthesizedAlignmentMethod();
	//        level4Alignment = level4SAM.computeMethod(onto1, onto2);
	//        
	//        //Synthesize level four alignment result to previous synthesized alignment level i.e. ThreeLevelAlignment
	//        //also trim to 0.8
	//        BasicAlignment FourLevelAlignment = (BasicAlignment) (level4Alignment.clone());
	//        FourLevelAlignment.ingest(ThreeLevelAlignment);
	//        
	//        
	//        //Xlevel alignment (Coclu lexical method)
	//        BasicAlignment levelXAlignment = new BasicAlignment();
	//        LevelXSynthesizedAlignmentMethod levelXSAM = new LevelXSynthesizedAlignmentMethod();
	//        levelXAlignment = levelXSAM.computeMethod(onto1, onto2);
	//        
	//        //Synthesize Xlevel alignment result to previous synthesized alignment level i.e. FourLevelAlignment
	//        //also trim to threshold
	//        BasicAlignment FiveLevelAlignment = (BasicAlignment) (FourLevelAlignment.clone());
	//        FiveLevelAlignment.ingest(levelXAlignment);
	//        
	//      
	//        //put everyting in the final alignment object BasicAlignment
	//        synthAlignment = (BasicAlignment) (FiveLevelAlignment.clone());
	//        //output for testsing execution
	//        System.out.println("setConf1SynthesizedAlignmentMethod: pass...");
	//
	//        return synthAlignment;
	//
	//    }
	//    
	//    //for ruSMART conference
	//    //confX for a synthesis method that uses eXperienced, eXperimental, alignment methods e.g. COCLU for names, labels, comments,
	//    public BasicAlignment setConfXSynthesizedAlignmentMethod(URI onto1, URI onto2) throws AlignmentException, IOException {
	//
	//        BasicAlignment synthAlignment = new BasicAlignment();
	//
	//        //First method alignment (COCLU names)
	//        BasicAlignment levelXAlignment = new BasicAlignment();
	//        LevelXSynthesizedAlignmentMethod levelXSAM = new LevelXSynthesizedAlignmentMethod();
	//        levelXAlignment = levelXSAM.computeMethod(onto1, onto2);
	//        
	//        //Second method alignment (COCLU labels)
	//        BasicAlignment level1LabelsAlignment = new BasicAlignment();
	//        Level1LabelsSynthesizedAlignmentMethod level1LabelsSAM = new Level1LabelsSynthesizedAlignmentMethod();
	//        level1LabelsAlignment = level1LabelsSAM.computeMethod(onto1, onto2);
	//
	//        //Synthesize first two methods
	//        BasicAlignment TwoLevelAlignment = (BasicAlignment) (levelXAlignment.clone());
	//        TwoLevelAlignment.ingest(level1LabelsAlignment);
	//        
	//        //third method alignment (COCLU comments)
	//        BasicAlignment level1CommentsAlignment = new BasicAlignment();
	//        Level1CommentsSynthesizedAlignmentMethod level1CommentsSAM = new Level1CommentsSynthesizedAlignmentMethod();
	//        level1CommentsAlignment = level1CommentsSAM.computeMethod(onto1, onto2);
	//        
	//         //Synthesize first two methods synthesized method with the third method
	//        BasicAlignment ThreeLevelAlignment = (BasicAlignment) (TwoLevelAlignment.clone());
	//        ThreeLevelAlignment.ingest(level1CommentsAlignment);
	//        
	//        //put everyting in the final alignment object BasicAlignment
	//        synthAlignment = (BasicAlignment) (ThreeLevelAlignment.clone());
	//        //output for testsing execution
	//        System.out.println("setConfXSynthesizedAlignmentMethod: pass...");
	//
	//        return synthAlignment;
	//
	//    }

}

