package fi.vtt.ict.IoT.ase.utils;

import fr.inrialpes.exmo.ontosim.string.JWNLDistances;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class JWNLDistancesOverride extends JWNLDistances{
	public String getGlossForLabelOverride( String s ){
		return getGlossForLabel(s);
	}
}
