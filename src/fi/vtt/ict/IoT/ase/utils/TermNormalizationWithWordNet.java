/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vtt.ict.IoT.ase.utils;

import java.util.StringTokenizer;

/**
 * Compound words handling for ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (12.05.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class TermNormalizationWithWordNet {

	//WordNetJDialog wjd;
	public TermNormalizationWithWordNet() {
		//wjd = new WordNetJDialog();
	}

	//    public String replaceTermWithWordNetEntry(String checkedTerm) throws JWNLException, FileNotFoundException {
	//
	//        String newTerm = "";
	//        //check if the term is compound
	//        boolean compoundTerm = checkIfCompoundTerm(checkedTerm);
	//        //if the term is compound, get the rightmost term as the most significant
	//        // replace the compound term with its rightmost term
	//        if (compoundTerm == true) {
	//            checkedTerm = replaceCompoundTermWithRightMostTerm(checkedTerm);
	//        }
	//        //check if term has entry in WordNet i.e. as a noun or as a verb        
	//        String WordNetPropFIleDir = "file_properties.xml";
	//        JWNL.initialize(new FileInputStream(WordNetPropFIleDir));
	//
	//        IndexWord wordNoun = Dictionary.getInstance().getIndexWord(POS.NOUN, checkedTerm);
	//        IndexWord wordVerb = Dictionary.getInstance().getIndexWord(POS.VERB, checkedTerm);
	//        //if term has not an entry in wordNet assign a new term from wordnet
	//        //needs to implement the selection of the WordNet term
	//        if ((wordNoun != null && wordNoun.equals("Thing") != true) || (wordVerb != null)) {
	//            newTerm = checkedTerm;
	//        } else {
	//            newTerm = assignWordNetTerm(checkedTerm);
	//        }
	//
	//        JWNL.shutdown();
	//        return newTerm;
	//    }

	//    public String assignWordNetTerm(String checkedTerm) {
	//        String wordNetTerm = "";
	//        //wjd.jTextField1.setText(checkedTerm);
	//        //wjd.setVisible(true);
	//        //test it
	//        //wordNetTerm="Token";
	//        //wordNetTerm=wjd.jTextField1.getText();
	//        //
	//
	//        return wordNetTerm;
	//    }
	//
	//    

	public static boolean checkIfCompoundTerm(String checkedTerm) {
		boolean isCompound = false;
		boolean hasCapitalLetter = false;
		//checkedTerm = "capital-city";
		//checkedTerm = "PatientTransport_ChosenHospital";
		//String compoundTerm ="_GOVERNMENT";

		char ch = ' ';
		for (int x = 0; x < checkedTerm.length(); x++) {
			ch = checkedTerm.charAt(x);
			if ((Character.isUpperCase(ch) == true) && (x > 1)) {
				ch = checkedTerm.charAt(x - 1);
				if (Character.isLowerCase(ch) == true) {
					hasCapitalLetter = true;
					break;
				}

			}
		}

		if ((checkedTerm.contains("-") == true || checkedTerm.contains("_") == true)
				&& (checkedTerm.indexOf("-") > 1 || checkedTerm.indexOf("_") > 1)
				|| (hasCapitalLetter == true)) {

			isCompound = true;
		}

		return isCompound;
	}

	public static String replaceCompoundTermWithRightMostTerm(String compoundTerm) {
		String RightMostTerm = "";
		String lastTerm = "";
		//String restTerm = "";
		//String bagOfWordsAsComments = "";

		if (compoundTerm.contains("-") == true) {
			lastTerm = compoundTerm.substring(compoundTerm.lastIndexOf("-") + 1, compoundTerm.length());
			//System.out.println("Last Term of compound Class=" + lastTerm);
		} else if (compoundTerm.contains("_") == true) {
			lastTerm = compoundTerm.substring(compoundTerm.lastIndexOf("_") + 1, compoundTerm.toString().length());
			//System.out.println("Last Term of compound Class=" + lastTerm);
		} else 
			lastTerm = compoundTerm;
		
		//tokenizer for the restTerm (left most part of the compound term)
		StringTokenizer tokens = new StringTokenizer(compoundTerm, ".,\";:?!\n\t\r~!@%^&*()+={}'[]///<> ,       ");
		String newCompoundTerm = "";
		while (tokens.hasMoreTokens()) {
			//run the stemmer on the current token
			String token = tokens.nextToken();
			newCompoundTerm = newCompoundTerm + token;
		}
		compoundTerm = newCompoundTerm;
		//System.out.println("Tokenized Compound Term =" + compoundTerm);
		// if (compoundTerm.contains("-") == true|| compoundTerm.contains("_") == true)
		//   restTerm = compoundTerm.substring(0, compoundTerm.indexOf(lastTerm) - 1);

		//System.out.println("Rest Term of compound Class=" + restTerm);

		RightMostTerm=lastTerm;

		//check if there is a capital letter following a sequence of small letters
		Boolean isLastTermCompound = false;
		char ch = ' ';
		int capitalLetterPos = 0;
		
		for (int x = lastTerm.length()-1; x >=0; x--) {
			ch = lastTerm.charAt(x);
			if (Character.isUpperCase(ch) == true) {
				ch = lastTerm.charAt(x - 1);
				if (Character.isLowerCase(ch) == true) {
					isLastTermCompound = true;
					//restTerm=lastTerm.substring(0, x);
					capitalLetterPos = x;
					break;
				}

			}
		}
		
		//if the lastTerm is compound, take the rightmost term seperated with the capital letter
		String lastTermOfCompundTerm = "";
		if (isLastTermCompound == true) {
			lastTermOfCompundTerm = lastTerm.substring(capitalLetterPos);
			//System.out.println("Last Term of compound Last Term=" + lastTermOfCompundTerm);
			RightMostTerm  = lastTermOfCompundTerm;
		}

		//bagOfWordsAsComments=restTerm+" "+RightMostTerm;
		//System.out.println("bagOfWordsAsComments:"+bagOfWordsAsComments);

		return RightMostTerm;
	}

	public static String replaceCompoundTermWithLeftMostTerm(String compoundTerm) {

		if(compoundTerm.startsWith("has")){
			char next = compoundTerm.charAt(3);
			if(next=='-' || next=='_' || Character.isUpperCase(next)){
				compoundTerm = compoundTerm.substring(3);
			}
		}

		String LeftMostTerm = "";
		String firstTerm = "";
		//String restTerm = "";
		//String bagOfWordsAsComments = "";

		if (compoundTerm.contains("-") == true) {
			firstTerm = compoundTerm.substring(0, compoundTerm.lastIndexOf("-"));
			//System.out.println("First Term of compound Class=" + firstTerm);
		} else if (compoundTerm.contains("_") == true) {
			firstTerm = compoundTerm.substring(0, compoundTerm.lastIndexOf("_"));
			//System.out.println("First Term of compound Class=" + firstTerm);
		} else 
			firstTerm=compoundTerm;
		//tokenizer for the restTerm (left most part of the compound term)
		StringTokenizer tokens = new StringTokenizer(compoundTerm, ".,\";:?!\n\t\r~!@%^&*()+={}'[]///<> ,       ");
		String newCompoundTerm = "";
		while (tokens.hasMoreTokens()) {
			//run the stemmer on the current token
			String token = tokens.nextToken();
			newCompoundTerm = newCompoundTerm + token;
		}
		compoundTerm = newCompoundTerm;
		//System.out.println("Tokenized Compound Term =" + compoundTerm);
		//if (compoundTerm.contains("-") == true|| compoundTerm.contains("_") == true)
		//  restTerm = compoundTerm.substring(firstTerm.length()+1,compoundTerm.length());

		//System.out.println("Rest Term of compound Class=" + restTerm);

		LeftMostTerm=firstTerm;

		//check if there is a capital letter following a sequence of small letters
		Boolean isFirstTermCompound = false;
		char ch = ' ';
		int capitalLetterPos = 0;
		for (int x = 0; x < firstTerm.length(); x++) {
			ch = firstTerm.charAt(x);
			if ((Character.isUpperCase(ch) == true) && (x > 1)) {
				ch = firstTerm.charAt(x - 1);
				if (Character.isLowerCase(ch) == true) {
					isFirstTermCompound = true;
					//restTerm=firstTerm.substring(0, x);
					capitalLetterPos = x;
					break;
				}

			}
		}
		//if the firstTerm is compound, take the leftmost term seperated with the capital letter
		String firstTermOfCompundTerm = "";
		if (isFirstTermCompound == true) {
			firstTermOfCompundTerm = firstTerm.substring(0, capitalLetterPos);
			//System.out.println("First Term of compound First Term=" + firstTermOfCompundTerm);
			LeftMostTerm=firstTermOfCompundTerm;
		}

		//bagOfWordsAsComments=restTerm+" "+LeftMostTerm;
		//System.out.println("bagOfWordsAsComments:"+bagOfWordsAsComments);

		return LeftMostTerm;
	}

	//Artem's addition
	public static String replacePrepositionWithTerm(String term){
		String lower = term.toLowerCase();
		if(lower.equals("to"))
			return "destination";
		else if(lower.equals("from"))
			return "origin";
		else if(lower.equals("in") || lower.equals("on") || lower.equals("at"))
			return "location";
		else if(lower.equals("since"))
			return "time";
		else if(lower.equals("since") || lower.equals("after"))
			return "start";
		else if(lower.equals("before") || lower.equals("until"))
			return "end";
		else if(lower.equals("for"))
			return "client";
		else if(lower.equals("by"))
			return "source";
		else if(lower.equals("about"))
			return "subject";
		else return term; 
	}
}
