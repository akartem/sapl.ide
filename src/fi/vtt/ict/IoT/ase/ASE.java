package fi.vtt.ict.IoT.ase;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Cell;

/**
 * ASE (Alignment of Smart Entities)
 * 
 * @author Konstantinos Kotis (VTT)
 * @author Artem Katasonov (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class ASE {

	public static String WORDNET_DICT = "";
	
/*	public static void main(String[] args) throws IOException, OntowrapException, URISyntaxException, AlignmentException, UnsupportedEncodingException, OWLOntologyCreationException, OWLOntologyStorageException, InterruptedException {

		// some tests with vectors and LUCENE
		//TestTermToTermMappingMethodUsingLucentVectors t1 =new TestTermToTermMappingMethodUsingLucentVectors();
		//TestClassToClassMappingUsingLSA t2 = new TestClassToClassMappingUsingLSA();

		//computation of ontologies similarity with ontologySim and Vector-based similarities       
		//TestOntoSimilarity t3 = new TestOntoSimilarity();
		//t3.kendall();
		//t3.cosine();
		//t3.initOnto();
		//t3.entityLexical();
		//t3.VectorSpaceCosineSim();
		//handle compound terms and terms with no entry in WordNet
		//"PatientTransport_ChosenHospital", "SecurityToken", "Motion_detected", "Switch-state";
		//System.out.println("isCompound:"+tnww.checkIfCompoundTerm("Switch-state"));
		//System.out.println("LeftMostTerm:"+tnww.replaceCompoundTermWithLeftMostTerm("Switch-state"));
		//System.out.println("LeftMostTerm:"+tnww.replaceCompoundTermWithRightMostTerm("Switch-state"));
		//tnww.replaceTermWithWordNetEntry("SecurityToken");
		//tnww.wait();


		//alignment of two ontologies                
		//URI onto1 = new URI("file:///C:/ontology1.owl");
		URI onto1 = new URI("file:///C:/ontology1.n3");
		URI onto2 = new URI("file:///C:/ontology2.n3");

		System.out.println("onto1:"+onto1);
		System.out.println("onto2:"+onto2);

		//run align methos
		
		try {
			Alignment output = align(onto1, onto2);
			//some output test
			//System.out.println(render(output));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
*/
	
	//our align method
	public static Alignment align(URI source, URI target) throws Exception {
		// Loading ontologies
		URI onto1 = source;
		URI onto2 = target;

		//translate onto if lang is not "en"

		/*boolean needForTranslation = false;
        TranslateOnto tOnt = new TranslateOnto();      
        needForTranslation = tOnt.checkOntoForENTranslationNeed(onto1);
        if (needForTranslation == true) {
            onto1 = tOnt.getTranslatedOnto(onto1, 1);
        }
        needForTranslation = tOnt.checkOntoForENTranslationNeed(onto2);
        if (needForTranslation == true) {
            onto2 = tOnt.getTranslatedOnto(onto2, 2);
        }*/

		//System.out.println("Onto1 URI before profiling starts:"+onto1);
		//System.out.println("Onto2 URI before profiling starts:"+onto2);

		//ontology profiling method - returns a specific profile of the ontologies to be aligned
		// input:two ontologies in URIs, output:profile in String e.g. 'profile1' which corresponds
		// to ontologies with small size, english labels, no instances, ontological richness, wordnet coverage.

		String alignmentConfiguration="";

		/*Taken out for ASE implementation
        String profile="";
        Profiling conf = new Profiling();
        profile = conf.getOntologiesProfile(onto1, onto2);

        System.out.println("Returned profile value:" + profile);
        //AUTOMSv2 configuration method - selects which alignment methods will be used based on the profile information
        //input: profile string, output:alignment Configuration string e.g. 'conf1' which corresponds
        // to specific pre-designed alignment methods' synthesis/configuration
        alignmentConfiguration = conf.setToolConfiguration(profile);
		 * 
		 */

		//System.out.println("Returned configuration value:" +alignmentConfiguration);

		//put resulted alignment in the final alignment object
		BasicAlignment finalAlignment = new BasicAlignment();

		Configuration synthesizedMethod = new Configuration();

		//for OAEI contest - must be taken for other uses
		alignmentConfiguration = "ASE";

		//System.out.println("Changed configuration value to:" +alignmentConfiguration);

		switch (alignmentConfiguration) {
		case "ASE":
			finalAlignment = synthesizedMethod.setConfAseSynthesizedAlignmentMethod(onto1, onto2);
			break;
			//            case "conf1":
			//                finalAlignment = synthesizedMethod.setConf1SynthesizedAlignmentMethod(onto1, onto2);
			//                break;                       
			//            case "ruSMART":
			//                finalAlignment = synthesizedMethod.setConfXSynthesizedAlignmentMethod(onto1, onto2);
			//                break;
			//            case "default":
			//                finalAlignment = synthesizedMethod.setConf1SynthesizedAlignmentMethod(onto1, onto2);
			//                break;
		}


		for(Cell cell : finalAlignment){
			System.out.println(cell.getObject1().toString()+" "+cell.getObject2().toString()+" "+cell.getStrength());
		}

		return finalAlignment;
	}

	public static String render(Alignment alignment) throws UnsupportedEncodingException, AlignmentException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStreamWriter out = new OutputStreamWriter(baos, "UTF-8");
		BufferedWriter bout = new BufferedWriter(out);
		String str = "";
		try (PrintWriter writer = new PrintWriter(bout)) {
			AlignmentVisitor renderer = new RDFRendererVisitor(writer);
			alignment.render(renderer);      
			writer.flush();
			str = baos.toString();
			writer.close();
		}
		return str;    	
	}

}
