/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.utils.JWNLDistancesOverride;
import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;
import fr.inrialpes.exmo.align.impl.MatrixMeasure;
import fr.inrialpes.exmo.ontosim.string.JWNLDistances;

import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;

/**
 * Wordnet synonyms right-most alignment methods of ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class JWNLAlignmentLabelsRightMostTerm extends AbstractAlignmentMethod {

    final static String WNVERS = "2.1";
    final static int BASICSYNDIST = 0;
    final static int COSYNSIM = 1;
    final static int BASICSYNSIM = 2;
    final static int WUPALMER = 3;
    final static int GLOSSOV = 4;

    //Artem's extension with gloss-based heuristics
//    final static int BASICPLUSSYNSIM = 5;
    
    final static double THRESHOLD = 0.85;

    protected class WordNetMatrixMeasure extends MatrixMeasure {

        protected JWNLDistances Dist = null;
        protected int method = 0;

        public WordNetMatrixMeasure() {
            similarity = false;
            //Dist = new JWNLDistances();
            Dist = new JWNLDistancesOverride();
        }

        public void init() throws AlignmentException {
            Dist.Initialize();
        }

        public void init(String wndict) throws AlignmentException {
            Dist.Initialize(wndict, WNVERS);
        }

        public void init(String wndict, String wnvers) throws AlignmentException {
            Dist.Initialize(wndict, wnvers);
        }

        public void init(String wndict, String wnvers, int simFunction) throws AlignmentException {

            Dist.Initialize(wndict, wnvers);
            method = simFunction;
        }

        public double measure(Object o1, Object o2) throws Exception {

            String s1 = getLabelOrName(ontology1(), o1); 
            String s2 = getLabelOrName(ontology2(), o2); 
            
            //Artem's addition
            s1 = TermNormalizationWithWordNet.replacePrepositionWithTerm(s1);
            s2 = TermNormalizationWithWordNet.replacePrepositionWithTerm(s2);
            
            if (s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty()) {
                return 0.0;
            }
            switch (method) {

                case COSYNSIM: {
                    double sim = Dist.cosynonymySimilarity(s1, s2);
                    return sim;
                }
                case BASICSYNSIM: {
                    double sim = Dist.basicSynonymySimilarity(s1, s2);
                    
                    //System.out.println(s1+" "+s2+" "+sim);
                    return sim;
                }
//                case BASICPLUSSYNSIM: {
//                    double sim = Dist.basicSynonymySimilarity(s1, s2);
//                    String gloss = ((JWNLDistancesOverride)Dist).getGlossForLabelOverride(s1);
//                    //System.out.println(gloss);
//                    
//                    if(gloss.contains(". a "+s2+" ") || gloss.contains(". an "+s2+" "))
//                    	sim = 0.95;  
//                    else if(gloss.contains(". the "+s2+" ")) 
//                    	sim = 0.9;
//                    else if(gloss.contains(" "+s2+" ")) 
//                    	sim = 0.85;
//                    
//                    System.out.println(s1+" "+s2+" "+sim);
//                    return sim;
//                }

                default:
                    return Dist.basicSynonymySimilarity(s1, s2);
            }

        }

        @Override
        public double classMeasure(Object cl1, Object cl2) throws Exception {
            return measure(cl1, cl2);
        }

        @Override
        public double propertyMeasure(Object pr1, Object pr2) throws Exception {
            return measure(pr1, pr2);
        }
      

        @Override
        public double individualMeasure(Object id1, Object id2) throws Exception {
            if (debug > 4) {
                System.err.println("ID:" + id1 + " -- " + id2);
            }
            return measure(id1, id2);
        }
    }

    /**
     * Creation *
     */
    public JWNLAlignmentLabelsRightMostTerm() {
        setSimilarity(new WordNetMatrixMeasure());
        setType("**");
    }

    ;

    /** Processing **/
    @Override
    public void align(Alignment alignment, Properties prop) throws AlignmentException {
        //System.out.println("in JWNLAlignmentLabels align method");
        int method = BASICSYNDIST;
        loadInit(alignment);
        WordNetMatrixMeasure sim = (WordNetMatrixMeasure) getSimilarity();
        String wnvers = prop.getProperty("wnvers");
        if (wnvers == null) {
            wnvers = WNVERS;
        }
        String function = prop.getProperty("wnfunction");
        if (function != null) {
            if (function.equals("cosynonymySimilarity")) {
                method = COSYNSIM;
            } else if (function.equals("basicSynonymySimilarity")) {
                method = BASICSYNSIM;
            } else if (function.equals("wuPalmerSimilarity")) {
                method = WUPALMER;
            } else if (function.equals("glossOverlapSimilarity")) {
                method = GLOSSOV;
	        } 
//            else if (function.equals("basicSynonymySimilarityPlus")) {
//	            method = BASICPLUSSYNSIM;
//	        }
        }
        sim.init(prop.getProperty("wndict"), wnvers, method);
        sim.initialize(ontology1(), ontology2(), alignment);
        //
        cleanUp();
        // Prepare the cache
        sim.Dist.initPreCache();
 
        //System.out.println("cache prepared");
        //compute class similarity
        try {
            // Match classes 
            
            for (Object cl1 : ontology1().getClasses()) {
                for (Object cl2 : ontology2().getClasses()) {
                    double simScore = sim.classMeasure(cl1, cl2);
                    if (simScore >= THRESHOLD) {
                        //System.out.println("cl1:" + cl1 + ", cl2" + cl2 + ", simScore:" + simScore);
                        // add mapping into alignment object 
                        addAlignCell(cl1, cl2, "=", simScore);
                    }
                }
            }

        } catch (Exception owex) {
             throw new AlignmentException("Error accessing ontology", owex);
        }
        //compute property similarity
        try {
            // Match classes 
            for (Object cl1 : ontology1().getProperties()) {
                for (Object cl2 : ontology2().getProperties()) {
                    double simScore = sim.propertyMeasure(cl1, cl2);
                    if (simScore >= THRESHOLD) {
                        //System.out.println("cl1:" + cl1 + ", cl2" + cl2 + ", simScore:" + simScore);
                        // add mapping into alignment object 
                        addAlignCell(cl1, cl2, "=", simScore);
                    }
                }
            }

        } catch (Exception owex) {
             throw new AlignmentException("Error accessing ontology", owex);
        }


        //
        sim.Dist.cleanPreCache();
        //System.out.println("cache cleaned");
        prop.setProperty("algName", getClass() + "/" + function);
        if (prop.getProperty("printMatrix") != null) {
            printDistanceMatrix(prop);
        }
        //extract(type, prop);
    }

	@Override
	protected String fixCompoundTerm(String input) {
		return TermNormalizationWithWordNet.replaceCompoundTermWithRightMostTerm(input).toLowerCase();
	}
}
