package fi.vtt.ict.IoT.ase.methods;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;

import java.net.URI;
import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Level5SynthesizedAlignmentMethod implements SynthesizedAlignmentMethod{

    public Level5SynthesizedAlignmentMethod() {
    }
    
//Instance-based alignment methods

    public BasicAlignment computeMethod(URI onto1, URI onto2) throws AlignmentException {

        BasicAlignment alignment = new BasicAlignment();
        Properties params = new BasicParameters();

        // 5th level of alignment methods // Run two different alignment
        //  methods for instance-based similarity 
 
        //first alignment method 
        AlignmentProcess a1 = new InstanceBasedAlignmentMethod();
        params.setProperty("debug","1");
        a1.init(onto1, onto2);
        a1.align((Alignment) null, params);
        a1.cut("hard", 0.98);
              
        alignment = (BasicAlignment) (a1.clone());
      
        return alignment;

    }

  }  