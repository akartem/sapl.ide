package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class EXcocluLabelsAlignmentMethodLeftMostTerm extends EXcocluLabelsAlignmentMethodRightMostTerm {
    @Override
	protected String fixCompoundTerm(String input){
    	return TermNormalizationWithWordNet.replaceCompoundTermWithLeftMostTerm(input).toLowerCase();
    }
}
