package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;
import fr.inrialpes.exmo.align.impl.MatrixMeasure;
import fr.inrialpes.exmo.ontosim.OntoSimException;
import fr.inrialpes.exmo.ontosim.string.JWNLDistances;

import java.util.*;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.IndexWordSet;
import net.didion.jwnl.data.PointerUtils;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;

import net.didion.jwnl.data.Synset;
import net.didion.jwnl.dictionary.Dictionary;

/**
 * Discovers a subsumption relation between two entities (labels) such as e1 > e2 
 * for ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */
 
public class JWNLAlignmentLabelsSubsum extends AbstractAlignmentMethod {

    final static String WNVERS = "2.1";
    final static int BASICSUMDIST = 0;
    
    final double ASSIGN_SCORE = 0.95; 
    
    protected class WordNetMatrixMeasure extends MatrixMeasure {

        protected JWNLDistances Dist = null;
        protected int method = 0;

        public WordNetMatrixMeasure() {
            similarity = false;
            Dist = new JWNLDistances();
        }

        public void init() throws AlignmentException {
            Dist.Initialize();
        }

        public void init(String wndict) throws AlignmentException {
            Dist.Initialize(wndict, WNVERS);
        }

        public void init(String wndict, String wnvers) throws AlignmentException {
            Dist.Initialize(wndict, wnvers);
        }

        public void init(String wndict, String wnvers, int simFunction) throws AlignmentException {

            Dist.Initialize(wndict, wnvers);
            method = simFunction;
        }

        public double measure(Object o1, Object o2) throws Exception {

            String s1 = getLabelOrName(ontology1(), o1); 
            String s2 = getLabelOrName(ontology2(), o2); 
            
            //Artem's addition
            s1 = TermNormalizationWithWordNet.replacePrepositionWithTerm(s1);
            s2 = TermNormalizationWithWordNet.replacePrepositionWithTerm(s2);

            if (s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty()) {
                return 0.0;
            }
            switch (method) {

                case BASICSUMDIST: {
                    double sim = basicSubsumptionSimilarity(s1, s2);
                    //System.out.println(s1+" "+s2+" "+sim);
                    return sim;
                }

                default:
                    return basicSubsumptionSimilarity(s1, s2);
            }

        }

        @Override
        public double classMeasure(Object cl1, Object cl2) throws Exception {
            return measure(cl1, cl2);
        }

        @Override
        public double propertyMeasure(Object pr1, Object pr2) throws Exception {
            return measure(pr1, pr2);
        }

        @Override
        public double individualMeasure(Object id1, Object id2) throws Exception {
            if (debug > 4) {
                System.err.println("ID:" + id1 + " -- " + id2);
            }
            return measure(id1, id2);
        }
    }

    /**
     * Creation *
     */
    public JWNLAlignmentLabelsSubsum() {
        setSimilarity(new WordNetMatrixMeasure());
        setType("**");
    }

    ;

    /** Processing **/
    @Override
    public void align(Alignment alignment, Properties prop) throws AlignmentException {
        //System.out.println("in JWNLAlignmentLabels align method");
        int method = BASICSUMDIST;
        loadInit(alignment);
        WordNetMatrixMeasure sim = (WordNetMatrixMeasure) getSimilarity();
        String wnvers = prop.getProperty("wnvers");
        if (wnvers == null) {
            wnvers = WNVERS;
        }
        String function = prop.getProperty("wnfunction");
        if (function != null) {
            if (function.equals("basicSubsumptionSimilarity")) {
                method = BASICSUMDIST;

            }
        }
        sim.init(prop.getProperty("wndict"), wnvers, method);
        sim.initialize(ontology1(), ontology2(), alignment);
        //
        cleanUp();
        // Prepare the cache
        sim.Dist.initPreCache();
        //System.out.println("cache prepared");
        //compute class similarity
        try {
            // Match classes 
            for (Object cl1 : ontology1().getClasses()) {
                for (Object cl2 : ontology2().getClasses()) {
                    double simScore = sim.classMeasure(cl1, cl2);
                    if (simScore >= ASSIGN_SCORE) {
                        //System.out.println("cl1:" + cl1 + ", cl2" + cl2 + ", simScore:" + simScore);
                        // add mapping into alignment object 
                        addAlignCell(cl1, cl2, ">&gt", simScore);
                    }
                }
            }

        } catch (Exception owex) {
            throw new AlignmentException("Error accessing ontology", owex);
        }
        //compute property similarity
        try {
            // Match classes 
            for (Object cl1 : ontology1().getProperties()) {
                for (Object cl2 : ontology2().getProperties()) {
                    double simScore = sim.propertyMeasure(cl1, cl2);
                    if (simScore >= ASSIGN_SCORE) {
                        //System.out.println("cl1:" + cl1 + ", cl2" + cl2 + ", simScore:" + simScore);
                        // add mapping into alignment object 
                        addAlignCell(cl1, cl2, "&gt", simScore);
                    }
                }
            }

        } catch (Exception owex) {
            throw new AlignmentException("Error accessing ontology", owex);
        }


        //
        sim.Dist.cleanPreCache();
        //System.out.println("cache cleaned");
        prop.setProperty("algName", getClass() + "/" + function);
        if (prop.getProperty("printMatrix") != null) {
            printDistanceMatrix(prop);
        }
        //extract(type, prop);
    }

    //discovers a subsumption relation between two strings such as s1 > s2, using wordnet lexicon
    public double basicSubsumptionSimilarity(String s1, String s2) throws JWNLException {
        double score = 0;

        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();        
       
        if (s1.equals(s2)) {
            return 0.;
        } else {
            // read synset for s1
            Set<Synset> synset1 = computeSynsets4Cache1(s1);
            //read all hyponym WordNet synsets for string s2
            Set<Synset> synset2 = computeSynsets4Cache2(s2);
            if (synset2 != null && synset1 != null) {
                for (Synset sense1 : synset1) {                    
                    for (Synset sense2 : synset2) {                        
                        Object[] hyponymList = PointerUtils.getInstance().getDirectHypernyms(sense2).toArray();                       
                        for (int k = 0; k < hyponymList.length; k++) {
                            String StrSense1 = sense1.toString();
                            if (hyponymList[k].toString().contains(StrSense1)) {
                                //System.out.println("S1 Sense: " + StrSense1);
                                //System.out.println("S2 Sense hyponym: " + hyponymList[k]);
                                //System.out.println("******SCORED******");
                                score = ASSIGN_SCORE;
                            }
                        }                       
                    }
                }
            }
        }
        return score;
    }//end function
    
    protected WeakHashMap cache1;
    protected WeakHashMap cache2;

    public void initPreCache() {
        cache1 = new WeakHashMap<String, Object>();
        cache2 = new WeakHashMap<String, Object>();
    }

    Set<Synset> computeSynsets4Cache1(String s) throws OntoSimException {
        String term = s.toLowerCase();
        if (cache1 != null && cache1.containsKey(term)) {
            return (Set<Synset>) cache1.get(term);
        } else {
            Set<Synset> sense = getAllSenses(term);
            if (cache1 != null) {
                cache1.put(term, sense);
            }
            return sense;
        }
    }

    Set<Synset> computeSynsets4Cache2(String s) throws OntoSimException {
        String term = s.toLowerCase();
        if (cache2 != null && cache2.containsKey(term)) {
            return (Set<Synset>) cache2.get(term);
        } else {
            Set<Synset> sense = getAllSenses(term);
            if (cache2 != null) {
                cache2.put(term, sense);
            }
            return sense;
        }
    }

    Set<Synset> getAllSenses(String term) throws OntoSimException {
        Set<Synset> res = new HashSet<Synset>();
        IndexWordSet iws = null;
        Dictionary dictionary = null;
        dictionary = Dictionary.getInstance();
        try {
            iws = dictionary.lookupAllIndexWords(term);
        } catch (JWNLException ex) {
            throw new OntoSimException("Wordnet exception", ex);
        }
        if (iws != null) {
            // not iterable...
            for (IndexWord idx : (Collection<IndexWord>) iws.getIndexWordCollection()) {
                Synset Syno[] = null;
                try {
                    // get the synsets for each sense
                    Syno = idx.getSenses();
                } catch (JWNLException jwnlex) {
                    throw new OntoSimException("Wordnet exception", jwnlex);
                    //jwnlex.printStackTrace();
                }
                // number of senses for the word s1
                int synonymNb = idx.getSenseCount();
                // for each sense
                for (int k = 0; k < synonymNb; k++) {
                    res.add(Syno[k]);
                }
            }
        }

        return res;
    }
    
	@Override
	protected String fixCompoundTerm(String input) {
		return TermNormalizationWithWordNet.replaceCompoundTermWithLeftMostTerm(input).toLowerCase();
	}
}
