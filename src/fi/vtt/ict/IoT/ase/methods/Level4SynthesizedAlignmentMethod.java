package fi.vtt.ict.IoT.ase.methods;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;

//import fr.inrialpes.exmo.align.impl.method.NameAndPropertyAlignment;
import java.net.URI;
import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Level4SynthesizedAlignmentMethod implements SynthesizedAlignmentMethod{

    public Level4SynthesizedAlignmentMethod() {
    }
    
//Structure-based alignment methods

    public BasicAlignment computeMethod(URI onto1, URI onto2) throws AlignmentException {

        BasicAlignment alignment = new BasicAlignment();
        Properties params = new BasicParameters();

        // 4th level of alignment methods // Run two different alignment
        //  methods for Structure-based class  similarity (e.g., ngram distance and smoa)
 /*
        //first alignment method 
        AlignmentProcess a1 = new NameAndPropertyAlignment();
        params.setProperty("debug","1");
        a1.init(onto1, onto2);
        a1.align((Alignment) null, params);
  */    
        //second alignment method 
        AlignmentProcess a2 = new StructureBasedAlignmentMethod() ;
        a2.init(onto1, onto2);
        params = new BasicParameters();
        params.setProperty("debug","1");
        a2.align((Alignment) null, params);
        double ssThreshold = 0.60;
        a2.cut("hard", ssThreshold);
   /*
        // synthesize the two results using simple aggregator
        ((BasicAlignment) a1).ingest(a2);
        // Trim at variable threshold 
        double ssThreshold = 0.90;
        a1.cut("hard", ssThreshold);
   */   
        alignment = (BasicAlignment) (a2.clone());
       
        return alignment;

    }

  }  