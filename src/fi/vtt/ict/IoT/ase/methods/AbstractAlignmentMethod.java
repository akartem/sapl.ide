package fi.vtt.ict.IoT.ase.methods;

import java.util.Set;

import org.semanticweb.owl.align.AlignmentProcess;

import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;
import fr.inrialpes.exmo.align.impl.DistanceAlignment;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public abstract class AbstractAlignmentMethod extends DistanceAlignment implements AlignmentProcess {

	protected String lang = "en";
		
	protected String getLabelOrName(LoadedOntology<Object> honto, Object entity){
        String str;
		Set<String> st1Set = null;
		try {
			st1Set = honto.getEntityNames(entity, lang);
	        if(st1Set.isEmpty()) st1Set = honto.getEntityNames(entity);
		} catch (OntowrapException e) {
			e.printStackTrace();
		}
        if(st1Set==null || st1Set.isEmpty()){
        	String name = entity.toString();
        	int ind = name.lastIndexOf("#");
        	if(ind==-1) ind = name.lastIndexOf("/");
        	if(ind==-1) str = name;
        	else str = name.substring(ind+1); 
        	if(str.endsWith(">")) str = str.substring(0,str.length()-1);
        }
        else str = st1Set.iterator().next();
        return normalizeString(str).toLowerCase();
	}
	
    protected String normalizeString(String inputString) {
        boolean isCompoundTerm = TermNormalizationWithWordNet.checkIfCompoundTerm(inputString);
        if (isCompoundTerm) return fixCompoundTerm(inputString);
        else return inputString;
    }
    
    protected abstract String fixCompoundTerm(String input);
}
