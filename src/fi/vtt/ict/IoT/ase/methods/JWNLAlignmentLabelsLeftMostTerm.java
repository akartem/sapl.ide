/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;

/**
 * Wordnet synonyms left-most alignment methods of ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class JWNLAlignmentLabelsLeftMostTerm extends JWNLAlignmentLabelsRightMostTerm {
	@Override
	protected String fixCompoundTerm(String input) {
		return TermNormalizationWithWordNet.replaceCompoundTermWithLeftMostTerm(input).toLowerCase();
	}
}
