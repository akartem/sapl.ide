package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.utils.TermNormalizationWithWordNet;

import java.util.Vector;
import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontosim.string.StringDistances;

import java.util.Iterator;

import ontology.owl.alignment.OntoCocluHuffmanMatch;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class EXcocluLabelsAlignmentMethodRightMostTerm extends AbstractAlignmentMethod {

	private HeavyLoadedOntology<Object> honto1 = null;
	private HeavyLoadedOntology<Object> honto2 = null;
	public double[][] lowerBoundConceptsSimilarity = null;
	public double[][] upperBoundConceptsSimilarity = null;
	public double[][] lowerBoundPropertiesSimilarity = null;
	public double[][] upperBoundPropertiesSimilarity = null;

	public EXcocluLabelsAlignmentMethodRightMostTerm() {
	}

	// Initialization
	// The class requires HeavyLoadedOntologies
	public void init(Object o1, Object o2, Object ontologies) throws AlignmentException {
		super.init(o1, o2, ontologies);
		if (!(getOntologyObject1() instanceof HeavyLoadedOntology
				&& getOntologyObject2() instanceof HeavyLoadedOntology)) {
			throw new AlignmentException(" requires HeavyLoadedOntology ontology loader");
		}
	}

	/**
	 * Processing *
	 */
	// Could better use similarity
	public void align(Alignment alignment, Properties params) throws AlignmentException {

		loadInit(alignment);
		honto1 = (HeavyLoadedOntology<Object>) getOntologyObject1();
		honto2 = (HeavyLoadedOntology<Object>) getOntologyObject2();
		//double threshold = 1.; // threshold above which distances are too high
		int i, j = 0;     // index for onto1 and onto2 classes
		int nbclass1 = 0; // number of classes in onto1
		int nbclass2 = 0; // number of classes in onto2
		Vector<Object> classlist2 = new Vector<Object>(10); // onto2 classes
		Vector<Object> classlist1 = new Vector<Object>(10); // onto1 classes
		double classmatrix[][];   // class distance matrix
		int nbprop1 = 0; // number of properties in onto1
		int nbprop2 = 0; // number of properties in onto2
		Vector<Object> proplist2 = new Vector<Object>(10); // onto2 properties
		Vector<Object> proplist1 = new Vector<Object>(10); // onto1 properties
		double propmatrix[][];   // properties distance matrix
		double pic1 = 0.5; // class weigth for name
		double pia1 = 0.5; // relation weight for name

		if (params.getProperty("debug") != null) {
			debug = Integer.parseInt(params.getProperty("debug"));
		}

		try {
			// Create property lists and matrix
			for (Object prop : honto1.getObjectProperties()) {
				nbprop1++;
				proplist1.add(prop);
			}
			for (Object prop : honto1.getDataProperties()) {
				nbprop1++;
				proplist1.add(prop);
			}
			for (Object prop : honto2.getObjectProperties()) {
				nbprop2++;
				proplist2.add(prop);
			}
			for (Object prop : honto2.getDataProperties()) {
				nbprop2++;
				proplist2.add(prop);
			}
			propmatrix = new double[nbprop1 + 1][nbprop2 + 1];
			
			// Create class lists
			for (Object cl : honto1.getClasses()) {
				nbclass1++;
				classlist1.add(cl);
			}
			for (Object cl : honto2.getClasses()) {
				nbclass2++;
				classlist2.add(cl);
			}
			classmatrix = new double[nbclass1 + 1][nbclass2 + 1];

//			System.out.println(classlist1);
//			System.out.println(classlist2);
			
//			System.out.println(proplist1);
//			System.out.println(proplist2);
			
			// Initialize property distances
			if (debug > 0) {
				System.err.println("Initializing property distances");
			}

			for (i = 0; i < nbprop1; i++) {
				Object cl1 = proplist1.get(i);
				String st1 = getLabelOrName(honto1, cl1);                

				for (j = 0; j < nbprop2; j++) {
					Object cl2 = proplist2.get(j);

					String st2 = getLabelOrName(honto2, cl2);   
					
					if (st1 != null && st2 != null) {
						propmatrix[i][j] = pia1 * StringDistances.levenshteinDistance(st1, st2);
					} else {
						propmatrix[i][j] = pia1;
					}

					//System.out.println(st1+" "+st2+" "+propmatrix[i][j]);
				}
			}

			// Initialize class distances
			if (debug > 0) {
				System.err.println("Initializing class distances");
			}
			for (i = 0; i < nbclass1; i++) {
				Object cl1 = classlist1.get(i);
				for (j = 0; j < nbclass2; j++) {
					Object cl2 = classlist2.get(j);

					String st1 = getLabelOrName(honto1, cl1);   
					String st2 = getLabelOrName(honto2, cl2); 

					if (st1 != null && st2 != null) {
						classmatrix[i][j] = pic1 * StringDistances.levenshteinDistance(st1.toLowerCase(), st2.toLowerCase());
					} else {
						classmatrix[i][j] = pic1;
					}
				}
			}
		} catch (OntowrapException owex) {
			throw new AlignmentException("Error accessing ontology", owex);
		}

		// Compute property distances using also coclu
		this.upperBoundPropertiesSimilarity = new double[proplist1.size()][proplist2.size()];
		this.lowerBoundPropertiesSimilarity = new double[proplist1.size()][proplist2.size()];
		OntoCocluHuffmanMatch cocluMHM1 = new OntoCocluHuffmanMatch(false, false);
		j = 0;
		Iterator propertyAtRowIter = proplist1.iterator();
		while (propertyAtRowIter.hasNext()) {
			Object ontProperty1 = propertyAtRowIter.next();

			String row_str = getLabelOrName(honto1, ontProperty1); 

			i = 0;
			Iterator propertyAtColumnIter = proplist2.iterator();
			while (propertyAtColumnIter.hasNext()) {
				Object ontProperty2 = propertyAtColumnIter.next();

				String col_str = getLabelOrName(honto2, ontProperty2); 

				//logic of Coclu algorithm

				//logic for how to treat concept matching
				if (row_str != null && col_str != null) {

					double val1Coclu = cocluMHM1.score(row_str.trim(), col_str.trim());
					double val2Coclu = cocluMHM1.score(col_str.trim(), row_str.trim());

					if (val1Coclu <= val2Coclu) //val1Coclu lowerBound
					{
						this.upperBoundPropertiesSimilarity[j][i] = val2Coclu;
						this.lowerBoundPropertiesSimilarity[j][i] = val1Coclu;
					} else //val2Coclu upperBound
					{
						this.upperBoundPropertiesSimilarity[j][i] = val1Coclu;
						this.lowerBoundPropertiesSimilarity[j][i] = val2Coclu;
					}
					//System.out.println("Properties: COCLU::(row,col)=("+row_str+","+col_str+")::-value1:"+val1Coclu+"--value2::"+val2Coclu);
				} else //no matching
				{
					this.upperBoundPropertiesSimilarity[j][i] = -50;
					this.lowerBoundPropertiesSimilarity[j][i] = -50;
				}
				//end logic
				i++;
			}//end 2nd while
			j++;
		}//end 1st while


		//normalize concept matrix for COCLU reuslts
		double[][] lowSimMatrix = this.lowerBoundPropertiesSimilarity;
		double[][] upperSimMatrix = this.upperBoundPropertiesSimilarity;
		int maxRow = proplist1.size();
		int maxCol = proplist2.size();
		//System.out.println("class list 1 size:"+maxRow);
		//System.out.println("class list 2 size:"+maxCol);
		for (i = 0; i < maxRow; i++) {
			for (j = 0; j < maxCol; j++) {
				double low = lowSimMatrix[i][j];
				double high = upperSimMatrix[i][j];
				if (low == 0.0 && high == 0.0) {
					//System.out.println("class matrix if low and high are 0:"+ propmatrix [i][j]);
					propmatrix[i][j] = 1;
				} else {
					//System.out.println("class matrix if else:"+ classmatrix [i][j]);
					propmatrix[i][j] = (double) (1 - ((-low - high) / 2));
				}
			}
		}

		if (debug > 0) {
			System.err.print("Storing property alignment\n");
		}
		for (i = 0; i < proplist1.size(); i++) {
			for (j = 0; j < proplist2.size(); j++) {
				double out = propmatrix[i][j];
				//System.out.println("add class cell:" + proplist1.get(i).toString() + proplist2.get(j).toString() + out);

				//System.out.println(proplist1.get(i)+" "+proplist2.get(j)+" "+out);
				
				addAlignCell(proplist1.get(i), proplist2.get(j), "=", out);
			}
		}

		// Compute classes distances using coclu
		if (debug > 0) {
			System.err.print("Computing class distances in eXcoclu\n");
		}

		this.upperBoundConceptsSimilarity = new double[classlist1.size()][classlist2.size()];
		this.lowerBoundConceptsSimilarity = new double[classlist1.size()][classlist2.size()];

		OntoCocluHuffmanMatch cocluMHM2 = new OntoCocluHuffmanMatch(false, false);
		j = 0;
		Iterator conceptAtRowIter = classlist1.iterator();
		while (conceptAtRowIter.hasNext()) {
			Object ontClass1 = conceptAtRowIter.next();
			String row_str = getLabelOrName(honto1, ontClass1); 

			i = 0;
			Iterator conceptAtColumnIter = classlist2.iterator();
			while (conceptAtColumnIter.hasNext()) {
				Object ontClass2 = conceptAtColumnIter.next();

				String col_str = getLabelOrName(honto2, ontClass2); 

				//logic of Coclu algorithm

				//logic for how to treat concept matching

				if (row_str != null && col_str!= null) {

					double val1Coclu = cocluMHM2.score(row_str.trim(), col_str.trim());
					double val2Coclu = cocluMHM2.score(col_str.trim(), row_str.trim());
					if (val1Coclu <= val2Coclu) //val1Coclu lowerBound
					{
						this.upperBoundConceptsSimilarity[j][i] = val2Coclu;
						this.lowerBoundConceptsSimilarity[j][i] = val1Coclu;
					} else //val2Coclu upperBound
					{
						this.upperBoundConceptsSimilarity[j][i] = val1Coclu;
						this.lowerBoundConceptsSimilarity[j][i] = val2Coclu;
					}
					//System.out.println(" Classes: COCLU::(row,col)=("+row_str+","+col_str+")::-value1:"+val1Coclu+"--value2::"+val2Coclu);
				} else //no matching
				{
					this.upperBoundConceptsSimilarity[j][i] = -50;
					this.lowerBoundConceptsSimilarity[j][i] = -50;
				}
				//end logic
				i++;
			}//end 2nd while
			j++;
		}//end 1st while

		//normalize concept matrix for COCLU reuslts
		double[][] lowSimMatrix2 = this.lowerBoundConceptsSimilarity;
		double[][] upperSimMatrix2 = this.upperBoundConceptsSimilarity;
		int maxRow2 = classlist1.size();
		int maxCol2 = classlist2.size();
		//System.out.println("class list 1 size:"+maxRow);
		//System.out.println("class list 2 size:"+maxCol);
		for (i = 0; i < maxRow2; i++) {
			for (j = 0; j < maxCol2; j++) {
				double low = lowSimMatrix2[i][j];
				double high = upperSimMatrix2[i][j];
				if (low == 0.0 && high == 0.0) {
					//System.out.println("class matrix if low and high are 0:"+ classmatrix [i][j]);
					classmatrix[i][j] = 1;
				} else {
					//System.out.println("class matrix if else:"+ classmatrix [i][j]);
					classmatrix[i][j] = (double) (1 - ((-low - high) / 2));
				}
			}
		}

		// Select the best match
		// There can be many algorithm for these:
		// n:m: get all of those above a threshold
		// 1:1: get the best discard lines and columns and iterate
		// Here we basically implement ?:* because the algorithm
		// picks up the best matching object above threshold for i.
		if (debug > 0) {
			System.err.print("Storing class alignment\n");
		}

		for (i = 0; i < classlist1.size(); i++) {
			for (j = 0; j < classlist2.size(); j++) {
				double out = classmatrix[i][j];
				//System.out.println("add class cell:" + classlist1.get(i).toString() + classlist2.get(j).toString() + out);
				addAlignCell(classlist1.get(i), classlist2.get(j), "=", out);
			}
		}

	}//end of align method

	@Override
	protected String fixCompoundTerm(String input) {
		return TermNormalizationWithWordNet.replaceCompoundTermWithRightMostTerm(input).toLowerCase();
	}

}
