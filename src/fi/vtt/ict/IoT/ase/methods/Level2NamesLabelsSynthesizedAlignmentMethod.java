package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.ASE;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.align.ling.JWNLAlignment;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;

/**
 * Wordnet synonyms alignment methods of ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Level2NamesLabelsSynthesizedAlignmentMethod implements SynthesizedAlignmentMethod{

	final static double THRESHOLD = 0.85;
    
	public Level2NamesLabelsSynthesizedAlignmentMethod() {
    }

    //WordNet-based alignment methods for synonyms mapping
    
    public BasicAlignment computeMethod(URI onto1, URI onto2) throws AlignmentException, IOException {

        Properties params = new BasicParameters();

        long s = System.currentTimeMillis();
        
        String WordNetDir = "";
        //String WordNetDir = System.getProperty("user.dir") + '\\' + "dict";
        //String WordNetDir = "/dict";
        //String WordNetDir = "dict";

        //String sep = System.getProperty("file.separator");
        //WordNetDir = new File("").getAbsolutePath() + sep + "dict" + sep;
        WordNetDir = ASE.WORDNET_DICT;
        System.out.println("WordNet path:" + WordNetDir);
  
        // methods for string similarity using WordNet (e.g.,cosynonymySimilarity, glossOverlapSimilarity) 

        //first alignment method for Labels
        AlignmentProcess b1 = new JWNLAlignmentLabelsRightMostTerm();
        params.setProperty("wndict", WordNetDir);
        params.setProperty("wnvers", "2.1");
        params.setProperty("wnfunction", "basicSynonymySimilarity");
                
        b1.init(onto1, onto2);
        b1.align(b1, params);

        long now = System.currentTimeMillis();
        System.out.println("Wordnet labels righttmost: "+((BasicAlignment) b1).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;
        
        //second alignment method Labels
        AlignmentProcess b2 = new JWNLAlignmentLabelsLeftMostTerm();
        params.setProperty("wndict", WordNetDir);
        params.setProperty("wnvers", "2.1");
        params.setProperty("wnfunction", "basicSynonymySimilarity");
        b2.init(onto1, onto2);
        b2.align(b2, params);

        now = System.currentTimeMillis();
        System.out.println("Wordnet labels lefttmost: "+((BasicAlignment) b2).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;

        // synthesize the two results using simple aggregator
        ((BasicAlignment) b1).ingest(b2);
        
         //third alignment method for Names
        AlignmentProcess b3 = new JWNLAlignment();
        params.setProperty("wndict", WordNetDir);
        params.setProperty("wnvers", "2.1");
        params.setProperty("wnfunction", "basicSynonymySimilarity");
        b3.init(onto1, onto2);
        b3.align(b3, params);
              
        now = System.currentTimeMillis();
        System.out.println("Wordnet names: "+((BasicAlignment) b3).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;
        
        // synthesize the two results using simple aggregator
        ((BasicAlignment) b1).ingest(b3);
        
        // Trim at variable threshold 
        b1.cut("hard", THRESHOLD);

        return (BasicAlignment) b1;

    }
}
