package fi.vtt.ict.IoT.ase.methods;

import java.net.URI;

import fr.inrialpes.exmo.align.impl.BasicAlignment;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public interface SynthesizedAlignmentMethod{
	public BasicAlignment computeMethod(URI onto1, URI onto2) throws Exception;
}
