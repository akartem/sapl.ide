/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vtt.ict.IoT.ase.methods;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;
import fr.inrialpes.exmo.align.impl.method.StringDistAlignment;

import java.net.URI;
import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;

/**
 * Lexical alignment methods of ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (12.05.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Level1NamesLabelsCommentsSynthesizedAlignmentMethod  implements SynthesizedAlignmentMethod{

    public Level1NamesLabelsCommentsSynthesizedAlignmentMethod() {
    }
    
    //String-based alignment methods

    public BasicAlignment computeMethod(URI onto1, URI onto2) throws AlignmentException {

        Properties params = new BasicParameters();
        double ssThreshold = 0.0;
        
        long s = System.currentTimeMillis();
             
        //first alignment method is Coclu with labels + right most term of a compound term
        AlignmentProcess a1 = new EXcocluLabelsAlignmentMethodRightMostTerm();
        params.setProperty("debug","0");
        a1.init(onto1, onto2);
        a1.align((Alignment) null, params);
        //ssThreshold = 0.978;
        ssThreshold = 0.95;
        a1.cut("hard", ssThreshold);
        
        long now = System.currentTimeMillis();
        System.out.println("Coclu labels rightmost: "+((BasicAlignment) a1).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;
        
        //second alignment method is Coclu with labels + left most term of a compound term
        AlignmentProcess a2 = new EXcocluLabelsAlignmentMethodLeftMostTerm();
        params.setProperty("debug","0");
        a2.init(onto1, onto2);
        a2.align((Alignment) null, params);
        //ssThreshold = 0.978;
        ssThreshold = 0.95;
        a2.cut("hard", ssThreshold);
        
        now = System.currentTimeMillis();
        System.out.println("Coclu labels leftmost: "+((BasicAlignment) a2).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;

        ((BasicAlignment) a1).ingest(a2);
        
        //third alignment method is for names similarity using SMOAN similarity method
        AlignmentProcess a3 = new StringDistAlignment();
        params.setProperty("stringFunction", "smoaDistance");
        a3.init(onto1, onto2);
        a3.align((Alignment) null, params);
        ssThreshold = 0.85;
        a3.cut("hard", ssThreshold);

        now = System.currentTimeMillis();
        System.out.println("SMOAN names: "+((BasicAlignment) a3).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;

        ((BasicAlignment) a1).ingest(a3);
        
        
//        //Fourth alignment method is Coclu for comments similarity
//        AlignmentProcess a4 = new EXcocluCommentsAlignmentMethod();
//        params.setProperty("debug","1");
//        a4.init(onto1, onto2);
//        a4.align((Alignment) null, params);
//        ssThreshold = 0.978;
//        a4.cut("hard", ssThreshold);
//
//        now = System.currentTimeMillis();
//        System.out.println("Coclu comments: "+((BasicAlignment) a4).getArrayElements().size()+" ["+(now-s)+"]");
//        s = now;
//
//        // synthesize the two results using simple aggregator
//        ((BasicAlignment) a1).ingest(a4);

        //synthesize to final alignment    
        return (BasicAlignment) a1;

    }

  }  