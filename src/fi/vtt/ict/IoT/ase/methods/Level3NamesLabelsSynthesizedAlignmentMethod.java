package fi.vtt.ict.IoT.ase.methods;

import fi.vtt.ict.IoT.ase.ASE;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.BasicParameters;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;

/**
 * Wordnet subsumption alignment methods of ASE (Alignment of Smart Entities)
 *
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class Level3NamesLabelsSynthesizedAlignmentMethod  implements SynthesizedAlignmentMethod{

	final static double THRESHOLD = 0.85;
	
	public Level3NamesLabelsSynthesizedAlignmentMethod() {
    }

    //WordNet-based alignment methods for subsumption mapping
    
    public BasicAlignment computeMethod(URI onto1, URI onto2) throws AlignmentException, IOException {

        BasicAlignment alignment = new BasicAlignment();
        //
        Properties params = new BasicParameters();
       
        String WordNetDir = "";
        //String WordNetDir = System.getProperty("user.dir") + '\\' + "dict";
        //String WordNetDir = "/dict";
        //String WordNetDir = "dict";

        //String sep = System.getProperty("file.separator");
        //WordNetDir = new File("").getAbsolutePath() + sep + "dict" + sep;      
        WordNetDir = ASE.WORDNET_DICT;
        //System.out.println("WordNet path:" + WordNetDir);
        
        long s = System.currentTimeMillis();
        
        //alignment method for labels subsumed by other labels (using WordNet) i.e. e1<e2
        AlignmentProcess b1 = new JWNLAlignmentLabelsSubsumed();
        params.setProperty("wndict", WordNetDir);
        params.setProperty("wnvers", "2.1");
        params.setProperty("wnfunction", "basicSubsumptionSimilarity");
        b1.init(onto1, onto2);
        b1.align(b1, params);
        
        long now = System.currentTimeMillis();
        System.out.println("Wordnet subsumed: "+((BasicAlignment) b1).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;
        
        //alignment method for labels that subsum other labels (using WordNet) i.e. e1> e2
        AlignmentProcess b2 = new JWNLAlignmentLabelsSubsum();
        params.setProperty("wndict", WordNetDir);
        params.setProperty("wnvers", "2.1");
        params.setProperty("wnfunction", "basicSubsumptionSimilarity");
        b2.init(onto1, onto2);
        b2.align(b2, params);

        now = System.currentTimeMillis();
        System.out.println("Wordnet subsume: "+((BasicAlignment) b2).getArrayElements().size()+" ["+(now-s)+"]");
        s = now;
        
         // synthesize the two results using simple aggregator
        ((BasicAlignment) b1).ingest(b2);
        
        b1.cut("hard", THRESHOLD);

        alignment = (BasicAlignment) (b1.clone());
        return alignment;

    }
}
