package fi.vtt.ict.IoT.ase.methods;

import fr.inrialpes.exmo.align.impl.DistanceAlignment;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.ontosim.string.StringDistances;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

import java.util.Set;
import java.util.Properties;
import java.util.Vector;

/**
 * @author Konstantinos Kotis (VTT)
 * 
 * @version 1.3 (16.04.2015)
 * 
 * @since 1.3
 * 
 * Copyright (C) 2015, VTT
 * 
 */

public class InstanceBasedAlignmentMethod extends DistanceAlignment implements AlignmentProcess {

    private HeavyLoadedOntology<Object> honto1 = null;
    private HeavyLoadedOntology<Object> honto2 = null;
    String lang = "en";

    public InstanceBasedAlignmentMethod() {
    }

    /**
     * Initialization The class requires HeavyLoadedOntologies
     */
    public void init(Object o1, Object o2, Object ontologies) throws AlignmentException {
        super.init(o1, o2, ontologies);
        if (!(getOntologyObject1() instanceof HeavyLoadedOntology
                && getOntologyObject1() instanceof HeavyLoadedOntology)) {
            throw new AlignmentException(" requires HeavyLoadedOntology ontology loader");
        }

    }

    // Could better use similarity
    public void align(Alignment alignment, Properties params) throws AlignmentException {

        loadInit(alignment);
        honto1 = (HeavyLoadedOntology<Object>) getOntologyObject1();
        honto2 = (HeavyLoadedOntology<Object>) getOntologyObject2();
        double threshold = 1.; // threshold above which distances are too high
        int i, j = 0;     // index for onto1 and onto2 classes
        int nbclass1 = 0; // number of classes in onto1
        int nbclass2 = 0; // number of classes in onto2
        Vector<Object> classlist2 = new Vector<>(10); // onto2 classes
        Vector<Object> classlist1 = new Vector<>(10); // onto1 classes
        double classmatrix[][];   // class distance matrix
        int nbprop1 = 0; // number of properties in onto1
        int nbprop2 = 0; // number of properties in onto2
        Vector<Object> proplist2 = new Vector<>(10); // onto2 properties
        Vector<Object> proplist1 = new Vector<>(10); // onto1 properties
        double propmatrix[][];   // properties distance matrix
        int nbinst1 = 0; // number of classes in onto1
        int nbinst2 = 0; // number of classes in onto2
        Vector<Object> instlist2 = new Vector<>(10); // onto2 classes
        Vector<Object> instlist1 = new Vector<>(10); // onto1 classes
        double instmatrix[][];   // class distance matrix
        double pic1 = 0.5; // class weigth for name
        double pic2 = 0.5; // class weight for properties
        double pia1 = 1.; // relation weight for name

        double epsillon = 0.05; // stoping condition


        if (params.getProperty("debug") != null) {
            debug = Integer.parseInt(params.getProperty("debug"));
        }

        try {



            // Create class lists
            for (Object cl : honto1.getClasses()) {
                nbclass1++;
                classlist1.add(cl);
            }
            for (Object cl : honto2.getClasses()) {
                nbclass2++;
                classlist2.add(cl);

            }

            classmatrix = new double[nbclass1 + 1][nbclass2 + 1];
          

//initialize instance distances
            // Create instance lists and matrix
            for (Object inst : honto1.getIndividuals()) {
                nbinst1++;
                instlist1.add(inst);
            }

            for (Object inst : honto2.getIndividuals()) {
                nbinst2++;
                instlist2.add(inst);
            }
            
            instmatrix = new double[nbinst1 + 1][nbinst2 + 1];

            if (debug > 0) {
                System.err.println("Initializing property distances");
            }

            for (i = 0; i < nbinst1; i++) {
                Object cl1 = instlist1.get(i);
                String st1 = honto1.getEntityName(cl1, lang);
                if (st1 != null) {
                    st1 = st1.toLowerCase();
                }
                for (j = 0; j < nbinst2; j++) {
                    Object cl2 = instlist2.get(j);
                    String st2 = honto2.getEntityName(cl2, lang);
                    if (st2 != null) {
                        st2 = st2.toLowerCase();
                    }
                    if (st1 != null || st2 != null) {
                        instmatrix[i][j] = pia1 * StringDistances.levenshteinDistance(st1, st2);
                    } else {
                        instmatrix[i][j] = pia1;
                    }
                }
            }
            
//initialize property distances
            // Create property lists and matrix
            for (Object prop : honto1.getObjectProperties()) {
                nbprop1++;
                proplist1.add(prop);
            }
            for (Object prop : honto1.getDataProperties()) {
                nbprop1++;
                proplist1.add(prop);
            }
            for (Object prop : honto2.getObjectProperties()) {
                nbprop2++;
                proplist2.add(prop);
            }
            for (Object prop : honto2.getDataProperties()) {
                nbprop2++;
                proplist2.add(prop);
            }

            propmatrix = new double[nbprop1 + 1][nbprop2 + 1];

            if (debug > 0) {
                System.err.println("Initializing property distances");
            }

            for (i = 0; i < nbprop1; i++) {
                Object cl1 = proplist1.get(i);
                String st1 = honto1.getEntityName(cl1, lang);
                if (st1 != null) {
                    st1 = st1.toLowerCase();
                }
                for (j = 0; j < nbprop2; j++) {
                    Object cl2 = proplist2.get(j);
                    String st2 = honto2.getEntityName(cl2, lang);
                    if (st2 != null) {
                        st2 = st2.toLowerCase();
                    }
                    if (st1 != null || st2 != null) {
                        propmatrix[i][j] = pia1 * StringDistances.levenshteinDistance(st1, st2);
                    } else {
                        propmatrix[i][j] = pia1;
                    }
                }
            }
//initialize class distances

            // Initialize class distances
            if (debug > 0) {
                System.err.println("Initializing class distances");
            }
            for (i = 0; i < nbclass1; i++) {
                Object cl1 = classlist1.get(i);
                for (j = 0; j < nbclass2; j++) {
                    classmatrix[i][j] = pic1 * StringDistances.levenshteinDistance(honto1.getEntityName(cl1).toLowerCase(), honto2.getEntityName(classlist2.get(j)).toLowerCase());
                }
            }

        } catch (OntowrapException owex) {
            throw new AlignmentException("Error accessing ontology", owex);
        }


        // Iterate until completion
        double factor = 1.0;
        while (factor > epsillon) {
            // Compute property distances
            // -- FirstExp: nothing to be done: one pass
            // Here create the best matches for property distance already
            // -- FirstExp: goes directly in the alignment structure
            //    since it will never be refined anymore...
            if (debug > 0) {
                System.err.print("Storing property alignment\n");
            }
            for (i = 0; i < nbprop1; i++) {
                boolean found = false;
                int best = 0;
                double max = threshold;
                for (j = 0; j < nbprop2; j++) {
                    if (propmatrix[i][j] < max) {
                        found = true;
                        best = j;
                        max = propmatrix[i][j];
                    }
                }
                if (found) {
                    addAlignCell(proplist1.get(i), proplist2.get(best), "=", 1. - max);
                }
            }

            if (debug > 0) {
                System.err.print("Computing class distances in Instance-based\n");
            }

            //compute instances alignment cells (after use must be taken out from alignment structure
            if (debug > 0) {
                System.err.print("Storing instance alignment\n");
            }
            for (i = 0; i < nbinst1; i++) {
                boolean found = false;
                int best = 0;
                double max = threshold;
                for (j = 0; j < nbinst2; j++) {
                    if (instmatrix[i][j] < max) {
                        found = true;
                        best = j;
                        max = instmatrix[i][j];
                    }
                }
                if (found) {
                    addAlignCell(instlist1.get(i), instlist2.get(best), "=", 1. - max);
                }
            }

            if (debug > 0) {
                System.err.print("Computing class distances in Instance-based\n");
            }

            // Compute classes distances based on instances alignment cells
            try {
                for (i = 0; i < nbclass1; i++) {
                    
                    Set<? extends Object> instances1 = honto1.getInstances(classlist1.get(i), OntologyFactory.ANY, OntologyFactory.ANY, OntologyFactory.ANY);
                    int nba1 = instances1.size();
                    if (nba1 > 0) { // if not, keep old values...
                        //Set correspondences = new HashSet();
                        for (j = 0; j < nbclass2; j++) {
                            
                            Set<? extends Object> instances2 = honto2.getInstances(classlist2.get(j), OntologyFactory.ANY, OntologyFactory.ANY, OntologyFactory.ANY);
                            int nba2 = instances2.size();
                            double attsum = 0.;
                            // check that there is a correspondance
                            // in list of class2 atts and add their weights
                            for (Object inst : instances1) {
                                Set<Cell> s2 = getAlignCells1(inst);
                                // Find the instance with the higest similarity
                                // that is matched here
                                double currentValue = 0.;
                                if (s2 != null) {
                                    for (Cell c2 : s2) {
                                        if (instances2.contains(c2.getObject2())) {
                                            double val = c2.getStrength();
                                            if (val > currentValue) {
                                                currentValue = val;
                                            }
                                        }
                                        //remove the instance align cell since it is not expected in an alignment structure
                                        removeAlignCell(c2);
                                    }
                                }

                                attsum = attsum + 1 - currentValue;
                            }
                            classmatrix[i][j] = classmatrix[i][j]
                                    + pic2 * (2 * attsum / (nba1 + nba2));
                        }
                    }
                }
            } catch (OntowrapException owex) {
                throw new AlignmentException("Error accessing ontology", owex);
            }



            // Assess factor
            // -- FirstExp: nothing to be done: one pass
            factor = 0.;
        }

        // Select the best match
        // There can be many algorithm for these:
        // n:m: get all of those above a threshold
        // 1:1: get the best discard lines and columns and iterate
        // Here we basically implement ?:* because the algorithm
        // picks up the best matching object above threshold for i.
        if (debug > 0) {
            System.err.print("Storing class alignment\n");
        }

        for (i = 0; i < nbclass1; i++) {
            boolean found = false;
            int best = 0;
            double max = threshold;
            for (j = 0; j < nbclass2; j++) {
                if (classmatrix[i][j] < max) {
                    found = true;
                    best = j;
                    max = classmatrix[i][j];
                }
            }
            if (found) {
                addAlignCell(classlist1.get(i), classlist2.get(best), "=", 1. - max);
            }
        }

    }//end of align method
}
